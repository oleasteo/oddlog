import sinon from "sinon";

import * as oddlog from "../lib/index";
import {createLogger, mixin} from "../lib/index";
import DummyWriteStream from "./_util/DummyWriteStream";

describe("plugin", () => {

  let logger;

  beforeEach(() => {
    logger = createLogger("some-id", {transports: [{stream: new DummyWriteStream()}]}, {hello: "world"});
  });
  afterEach((done) => logger.loggerScope.closed ? done() : logger.close(done));

  it("should call the plugin", () => {
    const plugin = sinon.spy();
    mixin(oddlog, plugin);
    mixin(oddlog, plugin);
    plugin.should.be.calledOnce();
    plugin.should.be.calledWith(oddlog);
  });

  it("should work with init call", () => {
    const plugin = {init: sinon.spy()};
    mixin(oddlog, plugin);
    plugin.init.should.be.calledWith(oddlog);
  });

  it("should throw on invalid input", () => {
    mixin.bind(null, oddlog, {}).should.throw();
  });

});
