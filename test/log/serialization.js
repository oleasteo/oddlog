import {createLogger} from "../../lib";
import {LIST} from "../../lib/constants/std-levels";
import {SCHEMA_VERSION} from "../../lib/classes/Log";

describe("log", () => {
  let logger, store;

  beforeEach(() => {
    store = [];
    logger = createLogger("some-id", {meta: {some: "meta"}, transports: [{store, level: 0}]}, {hello: "world"});
  });
  afterEach((done) => logger.loggerScope.closed ? done() : logger.close(done));

  it("should serialize proper source data for debugging", (done) => {
    const prevLogger = logger;
    logger = createLogger("some-id", {logSource: true, transports: [{store}]});
    logger
      .child("pew", {child: true})
      .info({msg: {available: true}}, "test log");
    const file = "/test/log/serialization.js", line = 19, row = 8;
    const log = store[0];
    const logRecordRaw = JSON.parse(log.getRecordString("raw"));
    const source = logRecordRaw[6];
    source.should.be.an.Array();
    source[0].should.endWith(file);
    source[1].should.be.equal(line);
    source[2].should.be.equal(row);
    prevLogger.close(done); // close previous logger
  });

  it("should serialize to a proper raw format", () => {
    logger.child("pew", {child: true}).info({msg: {available: true}}, "test log");
    const log = store[0];
    let raw = "[" + SCHEMA_VERSION;
    raw += ",\"_type\""; // typeKey
    raw += ",{\"some\":\"meta\"}"; // meta
    raw += ",\"some-id:pew\""; // logger name
    raw += "," + log.date; // timestamp
    raw += "," + LIST.indexOf("info"); // level
    raw += ",null"; // source
    raw += ",\"test log\""; // message
    raw += ",{\"msg\":{\"available\":true},\"child\":true,\"hello\":\"world\"}]"; // payload
    log.getRecordString("raw").should.be.equal(raw);
  });
});
