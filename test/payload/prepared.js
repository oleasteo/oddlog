import _ from "lodash";
import should from "should";

import {createLogger} from "../../lib/index";
import DummyWriteStream from "../_util/DummyWriteStream";

const FAKE_STREAMS = [{stream: new DummyWriteStream(false)}];
const PL0 = {some: "data", answer: 21}, PL1 = {lorem: "ipsum", answer: 42};
const MERGED_PAYLOADS = _.assign({}, PL0, PL1);

describe("payload prepare", () => {
  let loggerPayload0, loggerPayload1;
  beforeEach(() => {
    loggerPayload0 = _.cloneDeep(PL0);
    loggerPayload1 = _.cloneDeep(PL1);
  });

  describe("with root object", () => {
    let logger;
    beforeEach(() => { logger = createLogger("some-id", {transports: FAKE_STREAMS}, loggerPayload0); });

    it("should get cloned", () => {
      logger.getPreparedPayload().should
        .not.equal(loggerPayload0, "not root payload")
        .deepEqual(loggerPayload0);
    });

    it("should not get cloned with ownership", () => {
      let logger = createLogger("some-id", {transports: FAKE_STREAMS, ownPayload: true}, loggerPayload0);
      logger.getPreparedPayload().should.equal(loggerPayload0);
    });

    describe("and child object", () => {
      it("should get cloned", () => {
        let child = logger.child(loggerPayload1);
        child.getPreparedPayload().should
          .not.equal(loggerPayload0, "not root payload")
          .not.equal(loggerPayload1, "not child payload")
          .deepEqual(MERGED_PAYLOADS);
      });

      it("should not get cloned with ownership", () => {
        let logger = createLogger("some-id", {transports: FAKE_STREAMS, ownChildPayloads: true}, loggerPayload0)
          .child(loggerPayload1);
        logger.getPreparedPayload().should
          .equal(loggerPayload1)
          .deepEqual(MERGED_PAYLOADS);
        loggerPayload0.should.deepEqual(PL0);
      });

      it("should not get cloned with ownership via parameter", () => {
        let child = logger.child(true, loggerPayload1);
        child.getPreparedPayload().should
          .equal(loggerPayload1)
          .deepEqual(MERGED_PAYLOADS);
        loggerPayload0.should.deepEqual(PL0);
      });
    });

    describe("and child null", () => {
      it("should be null", () => { should(logger.child(null).getPreparedPayload()).be.null(); });
    });
  });

  describe("with root null", () => {
    let logger;
    beforeEach(() => { logger = createLogger("some-id", {transports: FAKE_STREAMS}, null); });

    it("should be null", () => { should(logger.getPreparedPayload()).be.null(); });

    describe("and child object", () => {
      let child;
      beforeEach(() => { child = logger.child(loggerPayload1); });

      it("should get cloned", () => {
        child.getPreparedPayload().should
          .not.equal(loggerPayload1, "not child payload")
          .deepEqual(loggerPayload1);
      });
    });

    describe("and child null", () => {
      it("should be null", () => { should(logger.child(null).getPreparedPayload()).be.null(); });
    });

    describe("and child undefined", () => {
      it("should be null", () => { should(logger.child().getPreparedPayload()).be.null(); });
    });
  });

  describe("with root undefined", () => {
    let logger;
    beforeEach(() => { logger = createLogger("some-id", {transports: FAKE_STREAMS}); });

    it("should be undefined", () => { should(logger.getPreparedPayload()).be.undefined(); });

    describe("and child object", () => {
      let child;
      beforeEach(() => { child = logger.child(loggerPayload1); });

      it("should get cloned", () => {
        child.getPreparedPayload().should
          .not.equal(loggerPayload1, "not child payload")
          .deepEqual(loggerPayload1);
      });
    });

    describe("and child null", () => {
      it("should be null", () => { should(logger.child(null).getPreparedPayload()).be.null(); });
    });

    describe("and child undefined", () => {
      it("should be undefined", () => { should(logger.child().getPreparedPayload()).be.undefined(); });
    });
  });
});
