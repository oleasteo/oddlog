import _ from "lodash";
import should from "should";

import {createLogger} from "../../lib/index";
import DummyWriteStream from "../_util/DummyWriteStream";

const FAKE_STREAMS = [{stream: new DummyWriteStream(false)}];
const PL0 = {some: "data", answer: 21}, PL1 = {lorem: "ipsum", answer: 42};
const MERGED_PAYLOADS = _.assign({}, PL0, PL1);

describe("payload merge", () => {
  let loggerPayload0, loggerPayload1;

  beforeEach(() => {
    loggerPayload0 = _.cloneDeep(PL0);
    loggerPayload1 = _.cloneDeep(PL1);
  });

  describe("object with object", () => {
    let logger;

    beforeEach(() => { logger = createLogger("some-id", {transports: FAKE_STREAMS}, loggerPayload0); });

    it("should result in a clone", () => {
      logger.info(loggerPayload1);
      let log = logger.__lastLog;
      log.getPayload().should
        .not.equal(loggerPayload0)
        .not.equal(loggerPayload1)
        .deepEqual(MERGED_PAYLOADS);
      loggerPayload0.should.deepEqual(PL0);
      loggerPayload1.should.deepEqual(PL1);
    });

    it("should result in the given object", () => {
      logger.info(true, loggerPayload1);
      let log = logger.__lastLog;
      log.getPayload().should
        .equal(loggerPayload1)
        .deepEqual(MERGED_PAYLOADS);
    });
  });

  describe("null with object", () => {
    let logger;

    beforeEach(() => { logger = createLogger("some-id", {transports: FAKE_STREAMS}, null); });

    it("should result in a clone", () => {
      logger.info(loggerPayload0);
      let log = logger.__lastLog;
      log.getPayload().should.not.equal(loggerPayload0).deepEqual(loggerPayload0);
    });

    it("should result in the given object", () => {
      logger.info(true, loggerPayload0);
      let log = logger.__lastLog;
      log.getPayload().should.equal(loggerPayload0);
    });

    it("should result in the given object via option", () => {
      logger = createLogger("some-id", {transports: FAKE_STREAMS, ownChildPayloads: true}, null);
      logger.info(loggerPayload0);
      let log = logger.__lastLog;
      log.getPayload().should.equal(loggerPayload0);
    });
  });

  describe("null with null", () => {
    it("should result in null", () => {
      let logger = createLogger("some-id", {transports: FAKE_STREAMS}, null);
      logger.info(null);
      should(logger.__lastLog.getPayload()).be.null();
      logger.info("some log");
      should(logger.__lastLog.getPayload()).be.null();
    });
  });

  describe("undefined with null", () => {
    it("should result in null", () => {
      let logger = createLogger("some-id", {transports: FAKE_STREAMS});
      logger.info(null, "some log");
      should(logger.__lastLog.getPayload()).be.null();
    });
  });

  describe("object with null", () => {
    it("should result in null", () => {
      let logger = createLogger("some-id", {transports: FAKE_STREAMS}, loggerPayload0);
      logger.info(null);
      should(logger.__lastLog.getPayload()).be.null();
    });
  });

  describe("undefined with undefined", () => {
    it("should result in undefined", () => {
      let logger = createLogger("some-id", {transports: FAKE_STREAMS});
      logger.info("some log");
      should(logger.__lastLog.getPayload()).be.undefined();
    });
  });
});
