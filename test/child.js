import DummyTransport from "./_util/DummyTransport";

import {createLogger} from "../lib/index";

describe("child", () => {
  let logger;

  beforeEach(() => {
    logger = createLogger("some-id", {transports: [{instance: new DummyTransport()}]}, {hello: "world"});
  });
  afterEach((done) => logger.loggerScope.closed ? done() : logger.close(done));

  it("should close the scope on close", (done) => {
    logger.info("test");
    logger.close(() => {
      logger.loggerScope.closed.should.be.true();
      done();
    });
  });

  it("should not close the scope without explicit close call", (done) => {
    logger.info("test");
    process.nextTick(() => {
      logger.loggerScope.closed.should.be.false();
      logger.close(done);
    });
  });

  it("should close the scope of named child on close of parent", (done) => {
    let child = logger.child("sub", {test: true});
    child.info("test");
    logger.close(() => {
      child.loggerScope.closed.should.be.true();
      done();
    });
  });

  it("should not close the scope of parent on close of named child", (done) => {
    let child = logger.child("sub", {test: true});
    child.info("test");
    child.close(() => {
      logger.loggerScope.closed.should.be.false();
      logger.close(done);
    });
  });

  it("should throw on messages of closed child", (done) => {
    let child = logger.child("sub", {test: true});
    child.close(false);
    child.info.bind(child, "test").should.throw();
    logger.close(done);
  });

  it("should not throw on message of logger with closed child", (done) => {
    let child = logger.child("sub", {test: true});
    child.close(false);
    logger.info.bind(logger, "test logger").should.not.throw();
    logger.close(done);
  });
});
