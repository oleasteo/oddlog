import {createLogger, ERROR, TRACE} from "../../lib/index";

describe("transports", () => {

  const INIT_MSG = "--- start cache ---";
  const END_MSG = "--- end cache ---";

  describe("TriggerTransport", () => {

    describe("with RayCache (silent window)", () => {
      let store, logger;

      beforeEach(() => {
        store = [];
        logger = createLogger("some-id", {
          transports: [{level: TRACE, threshold: ERROR, limit: 2, window: 2, silentWindow: true, transports: [{store}]}]
        }, {hello: "world"});
      });
      afterEach((done) => logger.loggerScope.closed ? done() : logger.close(done));

      it("should flood logs on trigger threshold", () => {
        logger.error("test above");
        shouldMatchLogMessages(store, "test above");
      });

      it("should not overreach the cache", () => {
        logger.info("test 0");
        logger.info("test 1");
        logger.info("test 2");
        logger.error("test above");
        shouldMatchLogMessages(store, "test 1", "test 2", "test above");
      });
    });

    describe("with RayCache (printed window)", () => {
      let store, logger;

      beforeEach(() => {
        store = [];
        logger = createLogger("some-id", {
          transports: [{level: TRACE, threshold: ERROR, limit: 2, window: 2, transports: [{store}]}]
        }, {hello: "world"});
      });
      afterEach((done) => logger.loggerScope.closed ? done() : logger.close(done));

      it("should flood logs on trigger threshold", () => {
        logger.error("test above");
        shouldMatchLogMessages(store, "test above");
      });

      it("should drop from cache according to limit and window", () => {
        logger.info("test 0");
        logger.info("test 1");
        logger.info("test 2");
        logger.info("test 3");
        logger.error("test above");
        shouldMatchLogMessages(store, "test 2", "test 3", "test above");
      });

      it("should flood the cache window", () => {
        logger.info("test 0");
        logger.info("test 1");
        logger.info("test 2");
        logger.error("test above");
        shouldMatchLogMessages(store, "test 0", "test 1", "test 2", "test above");
      });
    });

    describe("without replication", () => {
      let store, logger;

      beforeEach(() => {
        store = [];
        logger = createLogger("some-id", {
          transports: [{level: TRACE, threshold: ERROR, limit: 2, transports: [{store}]}]
        }, {hello: "world"});
      });
      afterEach((done) => logger.loggerScope.closed ? done() : logger.close(done));

      it("should not share its cache with children", () => {
        logger.transports[0]._scope.should.equal(logger.child().transports[0]._scope);
      });

      it("should not forward logs below trigger threshold level", () => {
        logger.verbose("test 0");
        store.length.should.be.equal(0);
        logger.info("test 1");
        store.length.should.be.equal(0);
      });

      it("should flood logs on trigger threshold", () => {
        logger.error("test above");
        shouldMatchLogMessages(store, "test above");
      });

      it("should not overreach the cache", () => {
        logger.info("test 0");
        logger.info("test 1");
        logger.info("test 2");
        logger.error("test above");
        shouldMatchLogMessages(store, "test 1", "test 2", "test above");
      });

      it("should flood its own and child logs", () => {
        logger.info("test 0");
        logger.child().info("test child");
        logger.info("test 2");
        logger.error("test above");
        shouldMatchLogMessages(store, "test child", "test 2", "test above");
      });
    });

    describe("with replication", () => {
      let store, logger;

      beforeEach(() => {
        store = [];
        logger = createLogger("some-id", {
          transports: [
            {level: TRACE, threshold: ERROR, limit: 2, replicate: true, transports: [{store}]}
          ]
        }, {hello: "world"});
      });
      afterEach((done) => logger.loggerScope.closed ? done() : logger.close(done));

      it("should not share its cache with children", () => {
        logger.transports[0]._scope.should.not.equal(logger.child().transports[0]._scope);
      });

      it("should not flood parent logs", () => {
        logger.info("test 0");
        logger.child().error("test child");
        shouldMatchLogMessages(store, "test child");
      });

      it("should not flood child logs", () => {
        logger.child().info("test child");
        logger.error("test above");
        shouldMatchLogMessages(store, "test above");
      });
    });

    describe("with limited replication", () => {
      let store, logger;

      beforeEach(() => {
        store = [];
        logger = createLogger("some-id", {transports: [{replicate: 2, transports: [{store}]}]}, {hello: "world"});
      });
      afterEach((done) => logger.loggerScope.closed ? done() : logger.close(done));

      it("should limit replication to the specified replication depth", () => {
        const child0 = logger.child();
        const child1 = child0.child();
        const child2 = child1.child();
        const child3 = child2.child();
        logger.transports[0]._scope.should.not.equal(child0.transports[0]._scope);
        child0.transports[0]._scope.should.not.equal(child1.transports[0]._scope);
        child1.transports[0]._scope.should.equal(child2.transports[0]._scope);
        child2.transports[0]._scope.should.equal(child3.transports[0]._scope);
      });

    });

  });

  function shouldMatchLogMessages(store, ...messages) {
    store.length.should.be.equal(messages.length + 2);
    INIT_MSG.should.be.equal(store[0].getFormattedMessage());
    for (let i = 0; i < messages.length; i++) { messages[i].should.be.equal(store[i + 1].getFormattedMessage()); }
    END_MSG.should.be.equal(store[messages.length + 1].getFormattedMessage());
  }

});
