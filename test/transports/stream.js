import {createLogger} from "../../lib/index";
import DummyWriteStream from "../_util/DummyWriteStream";

describe("transports", () => {

  describe("StreamTransport", () => {

    describe("stream without close privilege", () => {
      let streamEnded, stream, logger;

      beforeEach(() => {
        streamEnded = false;
        stream = new DummyWriteStream();
        stream.once("finish", () => streamEnded = true);
        logger = createLogger("some-id", {transports: [{stream}]}, {hello: "world"});
      });
      afterEach((done) => logger.loggerScope.closed ? done() : logger.close(done));

      it("should not end on close", (done) => {
        logger.info("test");
        logger.close(() => {
          streamEnded.should.be.false();
          done();
        });
      });

      it("of named child should equal the parent stream", (done) => {
        let child = logger.child("sub", {test: true});
        child.transports[0].stream.should.be.equal(stream);
        done();
      });
    });

    describe("stream with close privilege", () => {
      let streamEnded, stream, logger;

      beforeEach(() => {
        streamEnded = false;
        stream = new DummyWriteStream();
        stream.once("finish", () => streamEnded = true);
        logger = createLogger("some-id", {transports: [{stream, closeStream: true}]}, {hello: "world"});
      });
      afterEach((done) => logger.loggerScope.closed ? done() : logger.close(done));

      it("should end on close", (done) => {
        logger.info("test");
        logger.close(() => {
          streamEnded.should.be.true();
          done();
        });
      });

      it("should not end without explicit close", (done) => {
        logger.info("test");
        process.nextTick(() => {
          streamEnded.should.be.false();
          done();
        });
      });

      it("of named child should equal the parent stream", (done) => {
        let child = logger.child("sub", {test: true});
        child.transports[0].stream.should.be.equal(stream);
        done();
      });

      it("should not end on close of named child", (done) => {
        let child = logger.child("sub", {test: true});
        child.info("test");
        child.close(() => {
          streamEnded.should.be.false();
          done();
        });
      });
    });

  });

});
