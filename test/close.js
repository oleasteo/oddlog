import {createLogger} from "../lib/index";

describe("Logger", () => {
  let logger, store;

  beforeEach(() => {
    store = [];
    logger = createLogger("some-id", {transports: [{store}]});
  });
  afterEach((done) => logger.loggerScope.closed ? done() : logger.close(done));

  it("should ignore logs after close", (done) => {
    logger.info("test");
    logger.close(() => {
      store.length.should.be.equal(1);
      store[0].getFormattedMessage().should.be.equal("test");
      done();
    });
    logger.info("this should be ignored");
  });

  it("should throw on logs after close with failSilent=false", () => {
    logger.info("test");
    logger.close(false);
    logger.info.bind(logger, "this should be ignored").should.throw();
  });
});
