import should from "should";

import {createLogger} from "../../lib";
import {updateEnv} from "../../lib/services/env";

describe("logger", () => {
  describe("logSource", () => {
    let logger = null, store;

    afterEach((done) => logger == null || logger.loggerScope.closed ? done() : logger.close(done));

    it("should fallback to whether any env variable matches (matching debug)", () => {
      store = [];
      process.env.DEBUG = "some-id";
      updateEnv();
      logger = createLogger("some-id", {transports: [{store}]});
      logger.info("test");
      should(store[0]._source).not.be.null();
      Reflect.deleteProperty(process.env, "DEBUG");
      updateEnv();
    });

    it("should fallback to whether any env variable matches (matching trace)", () => {
      store = [];
      process.env.TRACE = "some-id";
      updateEnv();
      logger = createLogger("some-id", {transports: [{store}]});
      logger.info("test");
      should(store[0]._source).not.be.null();
      Reflect.deleteProperty(process.env, "TRACE");
      updateEnv();
    });

    it("should fallback to whether any env variable matches (not set)", () => {
      store = [];
      logger = createLogger("some-id", {transports: [{store}]});
      logger.info("test");
      should(store[0]._source).be.null();
    });

    it("should fallback to whether any env variable matches (not matching)", () => {
      store = [];
      process.env.DEBUG = "some-other-id";
      updateEnv();
      logger = createLogger("some-id", {transports: [{store}]});
      logger.info("test");
      should(store[0]._source).be.null();
      Reflect.deleteProperty(process.env, "DEBUG");
      updateEnv();
    });

    it("should be respected if set within logger options (matching, set to false)", () => {
      store = [];
      process.env.DEBUG = "some-id";
      updateEnv();
      logger = createLogger("some-id", {logSource: false, transports: [{store}]});
      logger.info("test");
      should(store[0]._source).be.null();
      Reflect.deleteProperty(process.env, "DEBUG");
      updateEnv();
    });

    it("should be respected if set within logger options (not matching, set to true)", () => {
      store = [];
      process.env.DEBUG = "some-other-id";
      updateEnv();
      logger = createLogger("some-id", {logSource: true, transports: [{store}]});
      logger.info("test");
      should(store[0]._source).not.be.null();
      Reflect.deleteProperty(process.env, "DEBUG");
      updateEnv();
    });
  });
});
