"use strict";

const Benchmark = require("benchmark");
const oddlog = require("../../");

const log0 = oddlog.createLogger("some-id", {transports: []});
const log1 = oddlog.createLogger("some-id", {transports: [{stream: process.stdout}]});
const log2 = oddlog.createLogger("some-id", {transports: [{stream: process.stdout}, {stream: process.stderr}]});

const suite = new Benchmark.Suite("oddlog/child");

const obj0 = {hello: "world"}, obj1 = {lorem: "ipsum", dolor: "sit", amet: obj0, truth: 42, date: new Date()};

suite
  .add("oddlog:child:0T-0P:", () => log0.child())
  .add("oddlog:child:0T-1P:", () => log0.child(obj0))
  .add("oddlog:child:0T-5P:", () => log0.child(obj1))
  .add("oddlog:child:1T-0P:", () => log1.child())
  .add("oddlog:child:1T-1P:", () => log1.child(obj0))
  .add("oddlog:child:1T-5P:", () => log1.child(obj1))
  .add("oddlog:child:2T-0P:", () => log2.child())
  .add("oddlog:child:2T-1P:", () => log2.child(obj0))
  .add("oddlog:child:2T-5P:", () => log2.child(obj1))
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});
