"use strict";

const _ = require("lodash");
const Benchmark = require("benchmark");
const oddlog = require("../../");
const DummyWriteStream = require("../_util/DummyWriteStream");

const suite = new Benchmark.Suite("oddlog/message-plain");

const fakeStream = new DummyWriteStream();

const logSilentObj0 = {
  transports: [{
    type: "trigger",
    immediateOwnership: false, // in real-world applications most logging commands should transfer ownership
    transports: [{stream: fakeStream}]
  }]
};
const logSilentObj1 = {
  transports: [{
    type: "trigger",
    immediateOwnership: false, // in real-world applications most logging commands should transfer ownership
    freeUp: false,
    transports: [{stream: fakeStream}]
  }]
};
const logPrintedObj = {
  transports: [{
    type: "trigger",
    immediateOwnership: false, // in real-world applications most logging commands should transfer ownership
    freeUp: false,
    // it's probably enabled in real-world applications, but for performance testing we consider it a legit disable:
    guard: false,
    transports: [{stream: fakeStream}]
  }]
};

const obj0 = {hello: "world"}, obj1 = {lorem: "ipsum", dolor: "sit", amet: obj0, truth: 42, date: new Date()};

const logSilent0 = oddlog.createLogger("some-id", logSilentObj0);
const logSilent1 = oddlog.createLogger("some-id", logSilentObj1);
const logPrinted = oddlog.createLogger("some-id", logPrintedObj);

const scopePrinted = logPrinted.transports[0]._scope;

logSilent0.info("initial message");
logSilent1.info("initial message");
for (let i = 0; i < scopePrinted.logs.length - 1; i++) { logPrinted.info("hello world"); }

suite
  .add("oddlog:transport-trigger:silent-0P:", () => logSilent0.info("hello world"))
  .add("oddlog:transport-trigger:silent-1P:", () => logSilent0.info(obj0, "hello world"))
  .add("oddlog:transport-trigger:silent-5P:", () => logSilent0.info(obj1, "hello world"))
  .add("oddlog:transport-trigger:silent-boost-0P:", () => logSilent1.info("hello world"))
  .add("oddlog:transport-trigger:silent-boost-1P:", () => logSilent1.info(obj0, "hello world"))
  .add("oddlog:transport-trigger:silent-boost-5P:", () => logSilent1.info(obj1, "hello world"))
  .add("oddlog:transport-trigger:printed-0P:", () => logPrinted.error("hello world"))
  .add("oddlog:transport-trigger:printed-1P:", () => logPrinted.error(obj0, "hello world"))
  .add("oddlog:transport-trigger:printed-5P:", () => logPrinted.error(obj1, "hello world"))
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});
