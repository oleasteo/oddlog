"use strict";

const Benchmark = require("benchmark");
const oddlog = require("../../");
const DummyWriteStream = require("../_util/DummyWriteStream");

const suite = new Benchmark.Suite("oddlog/create-and-message");

const fakeStream0 = new DummyWriteStream();
const fakeStream1 = new DummyWriteStream();

const log0Obj = {transports: []};
const log1Obj = {transports: [{stream: fakeStream0}]};
const log2Obj = {transports: [{stream: fakeStream0}, {stream: fakeStream1}]};
const obj0 = {hello: "world"}, obj1 = {lorem: "ipsum", dolor: "sit", amet: obj0, truth: 42, date: new Date()};

suite
  .add("oddlog:create-and-log:printed-0T-0P:", () => log0().info("hello world"))
  .add("oddlog:create-and-log:printed-0T-1P:", () => log0().info(obj0, "hello world"))
  .add("oddlog:create-and-log:printed-0T-5P:", () => log0().info(obj1, "hello world"))
  .add("oddlog:create-and-log:printed-1T-0P:", () => log1().info("hello world"))
  .add("oddlog:create-and-log:printed-1T-1P:", () => log1().info(obj0, "hello world"))
  .add("oddlog:create-and-log:printed-1T-5P:", () => log1().info(obj1, "hello world"))
  .add("oddlog:create-and-log:printed-2T-0P:", () => log2().info("hello world"))
  .add("oddlog:create-and-log:printed-2T-1P:", () => log2().info(obj0, "hello world"))
  .add("oddlog:create-and-log:printed-2T-5P:", () => log2().info(obj1, "hello world"))
  // .add("oddlog:create-and-log:silent-0T-0P:", () => log0().trace("hello world"))
  // .add("oddlog:create-and-log:silent-0T-1P:", () => log0().trace(obj0, "hello world"))
  // .add("oddlog:create-and-log:silent-0T-5P:", () => log0().trace(obj1, "hello world"))
  .add("oddlog:create-and-log:silent-1T-0P:", () => log1().trace("hello world"))
  .add("oddlog:create-and-log:silent-1T-1P:", () => log1().trace(obj0, "hello world"))
  .add("oddlog:create-and-log:silent-1T-5P:", () => log1().trace(obj1, "hello world"))
  // .add("oddlog:create-and-log:silent-2T-0P:", () => log2().trace("hello world"))
  // .add("oddlog:create-and-log:silent-2T-1P:", () => log2().trace(obj0, "hello world"))
  // .add("oddlog:create-and-log:silent-2T-5P:", () => log2().trace(obj1, "hello world"))
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

function log0() { return oddlog.createLogger("some-id", log0Obj); }

function log1() { return oddlog.createLogger("some-id", log1Obj); }

function log2() { return oddlog.createLogger("some-id", log2Obj); }
