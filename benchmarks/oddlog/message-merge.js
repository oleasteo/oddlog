"use strict";

const Benchmark = require("benchmark");
const oddlog = require("../../dist");
const DummyWriteStream = require("../_util/DummyWriteStream");

const suite = new Benchmark.Suite("oddlog/message-merge");

const fakeStream0 = new DummyWriteStream();
const fakeStream1 = new DummyWriteStream();

const log0Obj = {transports: []};
const log1Obj = {transports: [{stream: fakeStream0}]};
const log2Obj = {transports: [{stream: fakeStream0}, {stream: fakeStream1}]};
const obj0 = {hello: "world"}, obj1 = {lorem: "ipsum", dolor: "sit", amet: obj0, truth: 42, date: new Date()};
const pl0 = {got: "you"}, pl1 = {lorem: "dolor", some: "value", commit: "me", obj0, see: "ya"};

const log00 = oddlog.createLogger("some-id", log0Obj, pl0);
const log01 = oddlog.createLogger("some-id", log0Obj, pl1);
const log10 = oddlog.createLogger("some-id", log1Obj, pl0);
const log11 = oddlog.createLogger("some-id", log1Obj, pl1);
const log20 = oddlog.createLogger("some-id", log2Obj, pl0);
const log21 = oddlog.createLogger("some-id", log2Obj, pl1);

// Since the initial message might differ in performance (at least within oddlog it does given certain conditions)...
log00.info("initial message");
log01.info("initial message");
log10.info("initial message");
log11.info("initial message");
log20.info("initial message");
log21.info("initial message");

suite
  .add("oddlog:message-merge:printed-0T-1P-0P:", () => log00.info("hello world"))
  .add("oddlog:message-merge:printed-0T-1P-1P:", () => log00.info(obj0, "hello world"))
  .add("oddlog:message-merge:printed-0T-1P-5P:", () => log00.info(obj1, "hello world"))
  .add("oddlog:message-merge:printed-0T-5P-0P:", () => log01.info("hello world"))
  .add("oddlog:message-merge:printed-0T-5P-1P:", () => log01.info(obj0, "hello world"))
  .add("oddlog:message-merge:printed-0T-5P-5P:", () => log01.info(obj1, "hello world"))
  .add("oddlog:message-merge:printed-1T-1P-0P:", () => log10.info("hello world"))
  .add("oddlog:message-merge:printed-1T-1P-1P:", () => log10.info(obj0, "hello world"))
  .add("oddlog:message-merge:printed-1T-1P-5P:", () => log10.info(obj1, "hello world"))
  .add("oddlog:message-merge:printed-1T-5P-0P:", () => log11.info("hello world"))
  .add("oddlog:message-merge:printed-1T-5P-1P:", () => log11.info(obj0, "hello world"))
  .add("oddlog:message-merge:printed-1T-5P-5P:", () => log11.info(obj1, "hello world"))
  .add("oddlog:message-merge:printed-2T-1P-0P:", () => log20.info("hello world"))
  .add("oddlog:message-merge:printed-2T-1P-1P:", () => log20.info(obj0, "hello world"))
  .add("oddlog:message-merge:printed-2T-1P-5P:", () => log20.info(obj1, "hello world"))
  .add("oddlog:message-merge:printed-2T-5P-0P:", () => log21.info("hello world"))
  .add("oddlog:message-merge:printed-2T-5P-1P:", () => log21.info(obj0, "hello world"))
  .add("oddlog:message-merge:printed-2T-5P-5P:", () => log21.info(obj1, "hello world"))
  // .add("oddlog:message-merge:silent-0T-1P-0P:", () => log00.trace("hello world"))
  // .add("oddlog:message-merge:silent-0T-1P-1P:", () => log00.trace(obj0, "hello world"))
  // .add("oddlog:message-merge:silent-0T-1P-5P:", () => log00.trace(obj1, "hello world"))
  // .add("oddlog:message-merge:silent-0T-5P-0P:", () => log01.trace("hello world"))
  // .add("oddlog:message-merge:silent-0T-5P-1P:", () => log01.trace(obj0, "hello world"))
  // .add("oddlog:message-merge:silent-0T-5P-5P:", () => log01.trace(obj1, "hello world"))
  .add("oddlog:message-merge:silent-1T-1P-0P:", () => log10.trace("hello world"))
  .add("oddlog:message-merge:silent-1T-1P-1P:", () => log10.trace(obj0, "hello world"))
  .add("oddlog:message-merge:silent-1T-1P-5P:", () => log10.trace(obj1, "hello world"))
  .add("oddlog:message-merge:silent-1T-5P-0P:", () => log11.trace("hello world"))
  .add("oddlog:message-merge:silent-1T-5P-1P:", () => log11.trace(obj0, "hello world"))
  .add("oddlog:message-merge:silent-1T-5P-5P:", () => log11.trace(obj1, "hello world"))
  // .add("oddlog:message-merge:silent-2T-1P-0P:", () => log20.trace("hello world"))
  // .add("oddlog:message-merge:silent-2T-1P-1P:", () => log20.trace(obj0, "hello world"))
  // .add("oddlog:message-merge:silent-2T-1P-5P:", () => log20.trace(obj1, "hello world"))
  // .add("oddlog:message-merge:silent-2T-5P-0P:", () => log21.trace("hello world"))
  // .add("oddlog:message-merge:silent-2T-5P-1P:", () => log21.trace(obj0, "hello world"))
  // .add("oddlog:message-merge:silent-2T-5P-5P:", () => log21.trace(obj1, "hello world"))
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});
