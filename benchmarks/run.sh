#!/usr/bin/env bash

BASE_DIR="$(cd "`dirname "$0"`" && pwd)"
DEST_FILE="${BASE_DIR}"/results

[ -f "${DEST_FILE}" ] && rm "${DEST_FILE}"

for logger in oddlog bunyan pino; do
  for file in "${BASE_DIR}/${logger}/"*.js; do
    echo -en "\r${logger}: $(basename "${file}")             "
    node --max_old_space_size=8192 "${file}" >> "${DEST_FILE}"
  done
  echo -e "\r${logger}: done.              "
done

node "${BASE_DIR}/prepare_results.js"

# use https://www.chartgo.com for chart creation
