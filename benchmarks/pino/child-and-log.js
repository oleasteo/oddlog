"use strict";

const Benchmark = require("benchmark");
const pino = require("pino");
const DummyWriteStream = require("../_util/DummyWriteStream");

const fakeStream = new DummyWriteStream();

const log0 = pino({name: "some-id", safe: false, timestamp: true, enabled: false});
const log1 = pino({name: "some-id", safe: false, timestamp: true}, fakeStream);

const suite = new Benchmark.Suite("pino/child-and-log");

const obj0 = {hello: "world"}, obj1 = {lorem: "ipsum", dolor: "sit", amet: obj0, truth: 42, date: new Date()};

suite
  .add("pino:child-and-log:0T-0P:", () => log0.child({}).info("hello world"))
  .add("pino:child-and-log:0T-1P:", () => log0.child(obj0).info("hello world"))
  .add("pino:child-and-log:0T-5P:", () => log0.child(obj1).info("hello world"))
  .add("pino:child-and-log:1T-0P:", () => log1.child({}).info("hello world"))
  .add("pino:child-and-log:1T-1P:", () => log1.child(obj0).info("hello world"))
  .add("pino:child-and-log:1T-5P:", () => log1.child(obj1).info("hello world"))
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});
