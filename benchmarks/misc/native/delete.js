"use strict";

const Benchmark = require("benchmark");

const suiteOddLog = new Benchmark.Suite("reflect");

suiteOddLog
  .add("Reflect#delete      ", () => {
    let obj = {a: 42};
    Reflect.deleteProperty(obj, "a");
  })
  .add("delete#dot          ", () => {
    let obj = {a: 42};
    delete obj.a;
  })
  .add("delete#braces       ", () => {
    let obj = {a: 42};
    delete obj["a"];
  })
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

/*
node@10.6.0

  27M ops/s: dot, braces
  4.5M ops/s: Reflect
*/
