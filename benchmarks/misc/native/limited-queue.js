"use strict";

const Benchmark = require("benchmark");

const suiteOddLog = new Benchmark.Suite("limited-queue");

const LIMIT = 150, WINDOW_SIZE = 15, LIMIT_WINDOW = LIMIT + WINDOW_SIZE;

class LimitedQueue {
  constructor(limit) {
    this.first = this.last = null;
    this.length = 0;
    this.limit = limit;
  }

  enqueue(value) {
    const item = {value, next: null, prev: null};
    if (this.first === null) {
      this.first = this.last = item;
      this.length = 1;
    } else {
      if (this.length === this.limit) { (this.last = this.last.prev).next = null; } else { this.length++; }
      (item.next = this.first).prev = item;
      this.first = item;
    }
  }

  clear() {
    this.first = this.last = null;
    this.length = 0;
  }
}

let fullQueue = createFullQueue(), fullArray = createFullArray();

suiteOddLog
  .add("LimitedQueue#build", () => createFullQueue())
  .add("LimitedQueue#add-full", () => fullQueue.enqueue(LIMIT))
  .add("LimitedQueue#iterate-clear", () => {
    let queue = fullQueue;
    let item = queue.first;
    let sum = 0;
    while (item !== null) {
      sum += item.value;
      item = item.next;
    }
    queue.clear();
    return queue;
  })
  .add("ArrayQueue#build", () => createFullArray())
  .add("ArrayQueue#add-full0", () => {
    if (fullArray.length === LIMIT) { fullArray.shift(); }
    fullArray.push(LIMIT);
  })
  .add("ArrayQueue#add-full1", () => {
    if (fullArray.length === LIMIT_WINDOW) { fullArray.splice(0, WINDOW_SIZE); }
    fullArray.push(LIMIT);
  })
  .add("ArrayQueue#iterate-clear0", () => {
    let queue = fullArray, _len = queue.length;
    let sum = 0;
    for (let i = 0; i < _len; i++) { sum += queue[i]; }
    queue.splice(0, queue.length);
    return queue;
  })
  .add("ArrayQueue#iterate-clear1", () => {
    let queue = fullArray, _len = queue.length;
    let sum = 0;
    for (let i = 0; i < _len; i++) { sum += queue[i]; }
    return queue = [];
  })
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => {
    fullQueue = createFullQueue();
    fullArray = createFullArray();
    console.log(String(event.target));
  })
  // run async
  .run({async: true});

function createFullQueue() {
  let q = new LimitedQueue(LIMIT);
  for (let i = 0; i < LIMIT; i++) { q.enqueue(i); }
  return q;
}

function createFullArray() {
  let q = [];
  for (let i = 0; i < LIMIT; i++) { q.push(i); }
  return q;
}

// LimitedQueue performs about 30% worse during build and about 10% worse during add-full without window.
// With window, ArrayQueue add-full is about 4 times faster than LimitedQueue, thus ArrayQueue#add-full1 is best.
// The LimitedQueue iteration and clear performs about 4 times faster than ArrayQueue (with splice).
// ArrayQueue clear with splice is way faster than dropping the array and creating a new one.
// Update node@10.6.0: LQ ~20% worse for build; LQ ~30% better for add-full0; LQ ~250% worse for add-full1; LQ ~11x
//                     faster for iterate-clear
