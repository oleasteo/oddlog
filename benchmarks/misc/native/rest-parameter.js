"use strict";

const Benchmark = require("benchmark");

const __SLICE = Array.prototype.slice;

const suiteOddLog = new Benchmark.Suite("rest-parameter");

suiteOddLog
  .add("Single#0     ", () => single())
  .add("SingleSlice#0", () => singleSlice())
  .add("Single#1     ", () => single("hello"))
  .add("SingleSlice#1", () => singleSlice("hello"))
  .add("Single#2     ", () => single("hello", "world"))
  .add("SingleSlice#2", () => singleSlice("hello", "world"))
  .add("One#0        ", () => one())
  .add("OneSlice#0   ", () => oneSlice())
  .add("One#1        ", () => one("hello"))
  .add("OneSlice#1   ", () => oneSlice("hello"))
  .add("One#2        ", () => one("hello", "world"))
  .add("OneSlice#2   ", () => oneSlice("hello", "world"))
  .add("One#3        ", () => one("hello", "world", "lorem"))
  .add("OneSlice#3   ", () => oneSlice("hello", "world", "lorem"))
  .add("Multi#0      ", () => multi())
  .add("MultiSlice#0 ", () => multiSlice())
  .add("Multi#1      ", () => multi("hello", "world", "lorem", "ipsum", "dolor"))
  .add("MultiSlice#1 ", () => multiSlice("hello", "world", "lorem", "ipsum", "dolor"))
  .add("Multi#2      ", () => multi("hello", "world", "lorem", "ipsum", "dolor", "sit"))
  .add("MultiSlice#2 ", () => multiSlice("hello", "world", "lorem", "ipsum", "dolor", "sit"))
  .add("Multi#3      ", () => multi("hello", "world", "lorem", "ipsum", "dolor", "sit", "amet"))
  .add("MultiSlice#3 ", () => multiSlice("hello", "world", "lorem", "ipsum", "dolor", "sit", "amet"))
  // add listeners
  .on("error", err => console.error(err))
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

function single(...args) {
  return args;
}

function singleSlice() {
  return __SLICE.call(arguments, 0);
}

function one(x, ...args) {
  return args;
}

function oneSlice(x) {
  return __SLICE.call(arguments, 1);
}

function multi(x, y, z, a, b, ...args) {
  return args;
}

function multiSlice(x, y, z, a, b) {
  return __SLICE.call(arguments, 5);
}

// rest parameter wins in every case
