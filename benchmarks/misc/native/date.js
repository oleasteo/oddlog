"use strict";

const {performance} = require("perf_hooks");

const Benchmark = require("benchmark");

const suiteOddLog = new Benchmark.Suite("iteration");

suiteOddLog
  .add("Date.now        ", () => Date.now())
  .add("performance.now ", () => performance.now())
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

/*
node@10.7.0

  19.3M performance.now
  17.8M Date.now
*/
