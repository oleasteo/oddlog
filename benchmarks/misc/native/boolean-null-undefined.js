"use strict";

const Benchmark = require("benchmark");

const suiteOddLog = new Benchmark.Suite("boolean-null-undefined");

let boolFalse = false, boolTrue = true;
let objNull = null, objObject = {}, objUndef = void 0;
let undef = void 0;

let bool0, bool1, un;

suiteOddLog
  .add("Boolean#assign ", () => bool0 = boolFalse)
  .add("EqNull#assign  ", () => bool1 = objObject == null)
  .add("Boolean#false  ", () => boolFalse ? 4 : 2)
  .add("Boolean#true   ", () => boolTrue ? 2 : 4)
  .add("EqNull#=#false ", () => objObject == null ? 4 : 2)
  .add("EqNull#=#true  ", () => objNull == null ? 2 : 4)
  .add("EqNull#!#false ", () => objNull != null ? 4 : 2)
  .add("EqNull#!#true  ", () => objObject != null ? 2 : 4)
  .add("EqNull#=3#false", () => objObject === null ? 4 : 2)
  .add("EqNull#=3#true ", () => objNull === null ? 2 : 4)
  .add("EqNull#!3#false", () => objNull !== null ? 4 : 2)
  .add("EqNull#!3#true ", () => objObject !== null ? 2 : 4)
  .add("Undef#=#n#false", () => typeof objNull === "undefined" ? 4 : 2)
  .add("Undef#=#o#false", () => typeof objObject === "undefined" ? 4 : 2)
  .add("Undef#=#true   ", () => typeof objUndef === "undefined" ? 2 : 4)
  .add("Undef#!#n#true ", () => typeof objNull !== "undefined" ? 2 : 4)
  .add("Undef#!#o#true ", () => typeof objObject !== "undefined" ? 2 : 4)
  .add("Undef#!#false  ", () => typeof objUndef !== "undefined" ? 4 : 2)
  .add("Void0#=#n#false", () => typeof objNull === "undefined" ? 4 : 2)
  .add("Void0#=#o#false", () => objObject === void 0 ? 4 : 2)
  .add("Void0#=#true   ", () => objUndef === void 0 ? 2 : 4)
  .add("Void0#!#n#true ", () => objNull !== void 0 ? 2 : 4)
  .add("Void0#!#o#true ", () => objObject !== void 0 ? 2 : 4)
  .add("Void0#!#false  ", () => objUndef !== void 0 ? 4 : 2)
  .add("Void1#=#o#false", () => objObject === undef ? 4 : 2)
  .add("Void1#=#true   ", () => objUndef === undef ? 2 : 4)
  .add("Void1#!#n#true ", () => objNull !== undef ? 2 : 4)
  .add("Void1#!#o#true ", () => objObject !== undef ? 2 : 4)
  .add("Void1#!#false  ", () => objUndef !== undef ? 4 : 2)
  .add("Undef#assign   ", () => un = undef)
  .add("Void#assign    ", () => un = void 0)
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

// Comparison with undef variable is worst
// No significant differences between other ternary test cases
// Assignment of void 0 is even better than undef
