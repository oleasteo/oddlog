"use strict";

const Benchmark = require("benchmark");

const suiteOddLog = new Benchmark.Suite("typeof");

let bool = false;
let num = 42;

suiteOddLog
  .add("Boolean      ", () => bool ? 4 : 2)
  .add("Type#Boolean ", () => typeof bool === "boolean" ? 4 : 2)
  .add("Type#Function", () => typeof fn === "function" ? 4 : 2)
  .add("Type#Number  ", () => typeof num === "number" ? 4 : 2)
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

function fn() {}

// no significant difference
