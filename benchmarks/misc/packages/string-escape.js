/* eslint max-len: "off" */
"use strict";

const Benchmark = require("benchmark");
const fastJSON = require("fast-json-stringify");
const fastSafe = require("fast-safe-stringify");

const suiteOddLog = new Benchmark.Suite("json");

const fastJSONString = fastJSON({type: "string"});

let msg0 = "application startup";
let msg1 = "application \"startup\"";

suiteOddLog
  .add("a.Custom    ", () => "\"" + fastEscape(msg0) + "\"")
  .add("a.FastJSON  ", () => fastJSONString(msg0))
  .add("a.FastSafe  ", () => fastSafe(msg0))

  .add("b.Custom    ", () => "\"" + fastEscape(msg1) + "\"")
  .add("b.FastJSON  ", () => fastJSONString(msg1))
  .add("b.FastSafe  ", () => fastSafe(msg1))

  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

/*
node@10.7.0

  a)
    20.3M Custom
    19.2M FastJSON
     4.6M FastSafe
  b)
    6.8M Custom
    7.8M FastJSON
    4.5M FastSafe
*/

function fastEscape(str) {
  let result = "";
  let someEscape = false;
  let lastIdx = 0;
  let _len = str.length;
  for (let i = 0; i < _len; i++) {
    const code = str.charCodeAt(i);
    if (code === 34 || code === 92) {
      result += str.slice(lastIdx, i) + "\\";
      lastIdx = i;
      someEscape = true;
    } else if (code < 32) {
      result += str.slice(lastIdx, i);
      lastIdx = i + 1;
    }
  }
  return someEscape ? result + str.slice(lastIdx) : str;
}
