import {SIZE_FACTOR, TIME_FACTOR} from "../constants/units";

const UNIT_REGEX = /[a-zA-Z]*$/;

/*===================================================== Exports  =====================================================*/

export {sizeToBytes, timeToMillis};

/*==================================================== Functions  ====================================================*/

function sizeToBytes(size) {
  if (typeof size === "string") {
    let unit = UNIT_REGEX.exec(size)[0], value = +size.substr(0, size.length - unit.length);
    if (!SIZE_FACTOR.hasOwnProperty(unit) || Number.isNaN(value)) { return; }
    return value * SIZE_FACTOR[unit];
  } else if (typeof size === "number" && size >= 0) {
    return size;
  }
}

function timeToMillis(time) {
  if (typeof time === "string") {
    let unit = UNIT_REGEX.exec(time)[0], value = +time.substr(0, time.length - unit.length);
    if (!TIME_FACTOR.hasOwnProperty(unit) || Number.isNaN(value)) { return; }
    return value * TIME_FACTOR[unit];
  } else if (typeof time === "number") {
    return time;
  }
}
