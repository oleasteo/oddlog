import nsMatcher from "ns-matcher";

let DEBUG_FLAG_MATCH, TRACE_FLAG_MATCH;

updateEnv();

/*===================================================== Exports  =====================================================*/

export {
  updateEnv,
  isAnySet, isDebugSetAndTest, isTraceSetAndTest, isAnySetAndTest,
};

/*==================================================== Functions  ====================================================*/

function isAnySet() { return DEBUG_FLAG_MATCH != null || TRACE_FLAG_MATCH != null; }

function isDebugSetAndTest(name) { return DEBUG_FLAG_MATCH != null && DEBUG_FLAG_MATCH.test(name); }

function isTraceSetAndTest(name) { return TRACE_FLAG_MATCH != null && TRACE_FLAG_MATCH.test(name); }

function isAnySetAndTest(name) { return isDebugSetAndTest(name) || isTraceSetAndTest(name); }

function updateEnv() {
  DEBUG_FLAG_MATCH = process.env.DEBUG ? nsMatcher(process.env.DEBUG) : null;
  TRACE_FLAG_MATCH = process.env.TRACE ? nsMatcher(process.env.TRACE) : null;
}
