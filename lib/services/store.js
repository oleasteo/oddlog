import {createLogger} from "./logger";
import Logger from "../classes/Logger";

const MAP = {};
const _child = Logger.prototype.child;

/*===================================================== Exports  =====================================================*/

export {define, get};

/*==================================================== Functions  ====================================================*/

/**
 * Registers a logger to be available via {@link get}.
 *
 * @param {String|Logger} name The name of the logger to register. If a logger is provided, additional arguments are
 *        ignored.
 * @param {?Object|Logger} [options] The options to define the logger; see {@link createLogger}. If a logger is
 *        provided, it will be registered under the provided name; payload will be ignored.
 * @param {?Object} [payload] Payload to add to messages of the logger and its children.
 * @returns {Logger} The registered logger.
 */
function define(name, options, payload) {
  if (typeof name === "string") {
    return MAP[name] = options instanceof Logger ? options : createLogger(name, options, payload);
  }
  if (name instanceof Logger) { return MAP[name.loggerScope.name] = name; }
  throw new Error("Logger definition requires either a namespace string, or a Logger instance.");
}

/**
 * Retrieves a logger that has been registered via {@link define} previously.
 *
 * @param {String} name The name of the logger.
 * @param {?} [rest] If provided, a child logger will be created and returned; arguments are passed to
 *        {@link Logger#child}.
 * @returns {Logger} The retrieved logger.
 */
function get(name, ...rest) {
  if (!MAP.hasOwnProperty("name")) { throw new Error("Logger '" + name + "' unknown."); }
  return rest.length === 0 ? MAP[name] : Reflect.apply(_child, MAP[name], rest);
}
