import Logger from "./classes/Logger";
import Log from "./classes/Log";
import TriggerTransport from "./classes/transports/TriggerTransport";
import MemoryTransport from "./classes/transports/MemoryTransport";
import SplitTransport from "./classes/transports/SplitTransport";
import CapTransport from "./classes/transports/CapTransport";
import FileTransport from "./classes/transports/FileTransport";
import FileRotateTransport from "./classes/transports/FileRotateTransport";
import StreamTransport from "./classes/transports/StreamTransport";
import VoidTransport from "./classes/transports/VoidTransport";
import {createLogger} from "./services/logger";
import {mixin} from "./services/plugin";
import {createTransport, defineType} from "./services/transports";
import {define as defineLogger, get as getLogger} from "./services/store";
import {closeAll, eachScopeOnce} from "./utils/bulk-action";
import {MAPPED as LEVELS} from "./constants/std-levels";
import {assignAll as assign, clone, cloneDeep} from "./utils/object";
import * as STANDARD_TRANSFORMER from "./constants/std-transformer";
import packageJson from "../package.json";

/*===================================================== Exports  =====================================================*/

export const version = packageJson.version;

export default getLogger;

export {
  // main classes
  Logger, Log,
  // transport classes
  SplitTransport, StreamTransport, FileTransport, FileRotateTransport, CapTransport, TriggerTransport,
  MemoryTransport, VoidTransport,
  // creation functions
  createLogger, createTransport,
  // singleton functions
  getLogger, defineLogger,
  // utility functions
  assign, clone, cloneDeep,
  // misc functions
  defineType, eachScopeOnce, closeAll, mixin,
  // constants
  STANDARD_TRANSFORMER as stdTransformer,
  LEVELS as levels,
};

export const SILENT = LEVELS.silent;
export const TRACE = LEVELS.trace;
export const DEBUG = LEVELS.debug;
export const VERBOSE = LEVELS.verbose;
export const INFO = LEVELS.info;
export const WARN = LEVELS.warn;
export const ERROR = LEVELS.error;
export const FATAL = LEVELS.fatal;
