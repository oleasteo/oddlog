export {
  transformFunction as fn,
  transformError as err,
  transformIncomingMessage as req,
  transformServerResponse as res,
};

/*==================================================== Functions  ====================================================*/

/**
 * Creates an object providing type, name and length of the given function.
 *
 * @param {Function} fn The function to create a representative of.
 * @returns {Object} The object representation of the function.
 */
function transformFunction(fn) {
  if (typeof fn !== "function") { return fn; }
  return {
    type: "function",
    name: fn.name,
    length: fn.length
  };
}

/**
 * Creates an object providing message, name, code and stack of the given error.
 *
 * @param {Error} err The error to create a representative of.
 * @returns {Object} The object representation of the error.
 */
function transformError(err) {
  if (err == null || err.stack == null) { return err; }
  return {
    message: err.message,
    name: err.name,
    code: err.code,
    stack: err.stack
  };
}

/**
 * Creates an object providing method, url, headers, remote address and remote port of the incoming message.
 *
 * @param {http.IncomingMessage} req The incoming message to create a representative of.
 * @returns {Object} The object representation of the incoming message.
 */
function transformIncomingMessage(req) {
  let conn;
  if (req == null || (conn = req.connection) == null) { return req; }
  return {
    method: req.method,
    url: req.url,
    headers: req.headers,
    remoteAddress: conn.remoteAddress,
    remotePort: conn.remotePort
  };
}

/**
 * Creates an object providing status code and headers of the server response.
 *
 * @param {http.ServerResponse} res The server response to create a representative of.
 * @returns {Object} The object representation of the server response.
 */
function transformServerResponse(res) {
  if (res == null || res.statusCode == null) { return res; }
  //noinspection JSUnresolvedVariable
  return {
    statusCode: res.statusCode,
    headers: res._header
  };
}
