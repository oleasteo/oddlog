import NestedTransport from "./inherits/NestedTransport";
import BasicTransport from "./inherits/BasicTransport";
import {getLevelValue} from "../../services/levels";
import {MAPPED as LEVELS} from "../../constants/std-levels";

/*===================================================== Classes  =====================================================*/

/**
 * A NestedTransport class that filters messages above a given level.
 *
 * @class
 */
class SplitTransport extends BasicTransport {

  /**
   * Creates a new SplitTransport for the given logger with the given data scope.
   *
   * @param {!Object} options The options to pass to {@link BasicTransport} and {@link NestedTransport}.
   * @param {Function} createTransport A function to create new Transport instances from transport definition objects.
   * @param {Number[]} splitLevels The levels to split at.
   *
   * @param {Boolean} [createTransports] Passed to {@link NestedTransport}.
   * @constructor
   */
  constructor(options, createTransport, splitLevels, createTransports) {
    super(options, LEVELS.silent);
    NestedTransport.call(this, options, createTransport, createTransports, true, null);
    this.splitLevels = splitLevels;
  }

  /**
   * Creates a child SplitTransport instance.
   *
   * @param {Boolean} hasChildScope Whether the childLogger has a dedicated LoggerScope.
   * @returns {SplitTransport} The child Transport.
   */
  child(hasChildScope) {
    const child = new SplitTransport(this._options, this._createTransport, this.splitLevels, false);
    child.postChild(this, hasChildScope);
    return child;
  }

  /**
   * Forwards the log to nested transports according to the defined split thresholds.
   *
   * @param {Log} log The log to process.
   */
  write(log) {
    const splitLevels = this.splitLevels;
    for (let i = 0; i < splitLevels.length; i++) {
      if (log.level < splitLevels[i]) {
        this.transports[i].write(log);
        return;
      }
    }
    this.transports[splitLevels.length].write(log);
  }

  /**
   * Returns the minimum level of logs to be processed by this transport.
   * For this transport type, this will return `max(this.level, transports[0].minLevel())`.
   *
   * @returns {Number} The minimum level of logs to be processed by this transport.
   */
  minLevel() {
    const t0minLevel = this.transports[0].minLevel();
    return this.level < t0minLevel ? t0minLevel : this.level;
  }

}

SplitTransport.prototype.attach = NestedTransport.prototype.attach;
SplitTransport.prototype.postChild = NestedTransport.prototype.postChild;
SplitTransport.prototype.forceChild = NestedTransport.prototype.forceChild;

/*==================================================== Functions  ====================================================*/

/**
 * Creates a new SplitTransport according to given options.
 *
 * @param {!Object} options The options:
 * <ul>
 *   <li>`{(Number|String)[]}` split The levels to split at. The length must equal the length of `transports` minus one.
 *       Logs with a level between `thresholds[i-1]` (inclusive) and `thresholds[i]` (exclusive) will be passed to
 *       `transports[i]`; with `thresholds[-1] := 0`, `thresholds[thresholds.length] := Infinity`.</li>
 *   <li>Additional options as accepted by {@link NestedTransport}.</li>
 *   <li>Additional options as accepted by {@link BasicTransport}.</li>
 * </ul>
 * @param {Function} createTransport A function to create new Transport instances from transport definition objects.
 * @returns {SplitTransport} The created transport.
 */
function create(options, createTransport) {
  if (options.split.length !== options.transports.length - 1) {
    throw new Error("The amount of split levels must equal the amount of nested transports minus one.");
  }
  const split = options.split.map(getLvlValue);

  return new SplitTransport(options, createTransport, split, true);
}

function getLvlValue(nameOrValue) { return getLevelValue(nameOrValue, null); }

/*===================================================== Exports  =====================================================*/

export default SplitTransport;
export {create};
