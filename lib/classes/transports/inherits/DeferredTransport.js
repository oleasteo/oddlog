import {cloneDeep} from "../../../utils/object";

/*===================================================== Exports  =====================================================*/

/**
 * A Transport class abstraction that enables immediate action options for transports that defer messages.
 *
 * Implementations are expected to call {@link DeferredTransport#preWrite} within `Transport#write()` when the write is
 * going to be deferred.
 *
 * @class
 */
export default DeferredTransport;

/*==================================================== Functions  ====================================================*/

/**
 * Creates a new DeferredTransport with given options for deferred transports.
 *
 * Immediate actions for logs can affect the performance significantly since those actions are applied to all logs,
 * discarding lazy evaluation.
 *
 * @param {!Object} options The options:
 * <ul>
 *   <li>`{Boolean} [immediateFormatting=false]` Whether formatting should get applied to primary message texts
 *       immediately.</li>
 *   <li>`{Boolean} [immediateOwnership=true]` Whether message payloads that ain't owned by the logger should get
 *       shallow cloned immediately.</li>
 *   <li>`{Boolean} [immediateTransform=false]` Whether message payloads should get transformed immediately. Implies
 *       `immediateOwnership`.</li>
 *   <li>`{Boolean} [immediateStringify=false]` Whether the string representation of the log record should get created
 *       immediately. This makes the string representations of log records - as utilized by all default transports -
 *       invariant. Implies `immediateFormatting`, `immediateOwnership` and `immediateTransform`.</li>
 *   <li>`{Boolean} [immediateFreeze=false]` Whether message payloads should get deeply cloned immediately. While this
 *       makes the payload immune to any modification until the message delivery, this can also slow down your
 *       application significantly. For transports that don't make use of the message *objects*, consider
 *       `immediateStringify` instead. Implies `immediateFormatting`, `immediateOwnership` and
 *       `immediateTransform`.</li>
 * </ul>
 * @constructor
 */
function DeferredTransport(options) {
  let formatting, ownership, transform, stringify, freeze;
  formatting = !!options.immediateFormatting;
  freeze = options.hasOwnProperty("immediateFreeze") ? !!options.immediateFreeze : false;
  ownership = !freeze && !options.hasOwnProperty("immediateOwnership") || !!options.immediateOwnership;
  transform = !freeze && !!options.immediateTransform;
  stringify = !!options.immediateStringify;
  this.immediateFormatting = formatting;
  this.immediateTransform = transform;
  this.immediateOwnership = ownership;
  this.immediateStringify = stringify;
  this.immediateFreeze = freeze;
}

/**
 * Prepares the given log according to options. Needs to be called within `Transport#write()` of implementing classes.
 *
 * @param {Log} log The log to prepare.
 * @returns {Log} The prepared log.
 */
DeferredTransport.prototype.preWrite = function (log) {
  if (this.immediateFreeze) {
    let payload = log.getPayload();
    if (payload != null) {
      if (Array.isArray(payload)) { // see benchmarks
        let _len = payload.length, i;
        for (i = 0; i < _len; i++) { payload[i] = cloneDeep(payload[i]); }
      } else {
        for (let key in payload) {
          // eslint-disable-next-line max-depth
          if (payload.hasOwnProperty(key)) { payload[key] = cloneDeep(payload[key]); }
        }
      }
    }
  } else if (this.immediateTransform) {
    log.getPayload();
  } else if (this.immediateOwnership) {
    log.gainPayloadOwnership();
  }
  if (this.immediateStringify) { log.getRawRecordString(); }
  if (this.immediateFormatting) { log.getFormattedMessage(); }
  return log;
};
