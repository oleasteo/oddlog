import fs from "fs";
import {EOL} from "os";

import StreamTransport from "./StreamTransport";

/*===================================================== Classes  =====================================================*/

/**
 * A Transport class that writes messages into a file.
 *
 * @class
 */
class FileTransport extends StreamTransport {

  /**
   * Creates a new FileTransport with given options.
   *
   * @param {!Object} options The options to pass to {@link StreamTransport}.
   * @param {String} path The file path to write to.
   * @constructor
   */
  constructor(options, path) {
    const stream = fs.createWriteStream(path, {flags: "a"});
    // todo remove duplication with StreamTransport.create
    const format = options.format;
    const separator = options.hasOwnProperty("separator") ? options.separator : EOL;
    const encoding = options.encoding;
    super(options, stream, true, format || "raw", separator, encoding);
    this.stream.on("error", (err) => this.logger._error(err));
  }

}

/*==================================================== Functions  ====================================================*/

/**
 * Creates a new FileTransport according to given options.
 *
 * @param {!Object} options The options:
 * <ul>
 *   <li>`{String} path` The file path to write to.</li>
 *   <li>Additional options as accepted by {@link StreamTransport.create}. Except for `stream`, `closeStream` and
 *       `format`.</li>
 * </ul>
 * @returns {FileTransport} The created transport.
 */
function create(options) {
  const path = options.path;

  return new FileTransport(options, path);
}

/*===================================================== Exports  =====================================================*/

export default FileTransport;
export {create};
