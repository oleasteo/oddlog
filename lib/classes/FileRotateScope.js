import fs from "fs";
import EventEmitter from "events";

import {once} from "../utils/function";

/*===================================================== Classes  =====================================================*/

/**
 * A scope class that keeps an active stream to one file within a rotation environment.
 *
 * The following Events are emitted:
 *
 *  * "ready" - When the stream is opened and file statistics have been retrieved.
 *  * "failed" - When the stream has failed to open.
 *
 * @class
 * @private
 */
class FileRotateScope extends EventEmitter {

  /**
   * Creates a new scope.
   *
   * @param {String} file The path to the file to connect with.
   * @param {String} encoding The file encoding.
   * @param {Object} limits Limits to use for decisions. Can hold `{Number} [maxAge]` in seconds and/or
   *        `{Number} [maxSize]` in bytes.
   * @constructor
   */
  constructor(file, encoding, limits) {
    super();
    this.ready = false;
    this.closing = false;
    this.failed = false;
    this.finished = false;
    this.error = null;
    this.file = file;
    this.stream = null;
    this.size = null;
    this.maxDate = null;
    this.encoding = encoding;
    this.maxAge = limits.maxAge;
    this.maxSize = limits.maxSize;
  }

  /**
   * Decides whether a rotation should be triggered instead of writing the message to the active stream.
   *
   * @param {Date} messageDate The date of the message that is going to be logged next.
   * @param {Number} messageSize The size (in bytes) of the message that is going to be logged next.
   * @returns {Boolean} Whether a rotation should get triggered before logging.
   */
  shouldRotate(messageDate, messageSize) {
    // check maximum file age
    if (this.maxDate !== null && messageDate.getTime() > this.maxDate) { return true; }
    // check maximum file size
    if (this.maxSize != null && this.size + messageSize > this.maxSize) {
      return this.size > 0; // always allow at least one message per file
    }
    return false;
  }

  /**
   * Initiates the opening of the file stream.
   * This method creates the file stream and collects file statistics needed for decision making before calling the
   * callback.
   *
   * @param {Function} cb The callback.
   */
  open(cb) {
    let self = this;
    this._checkStats((err) => {
      if (err != null) {
        if (err.code === "ENOENT") {
          return this._createStream((err) => {
            if (err != null) { return next(err); }
            this._checkStats((err) => {
              if (err != null) {
                this.closing = true;
                this.stream.end();
              }
              next(err);
            });
          });
        } else {
          return next(err);
        }
      }
      this._createStream(next);
    });

    function next(err) {
      if (err != null) {
        self.failed = true;
        self.error = err;
        self.emit("failed", err);
        return cb(err);
      }
      self.ready = true;
      self.emit("ready");
      cb();
    }
  }

  /**
   * Ends any underlying stream and calls the callback when done.
   *
   * @param {Function} cb The callback to call when it is ensured that no underlying stream is open.
   */
  endStream(cb) {
    if (this.failed || this.finished) {
      cb();
    } else if (this.ready || this.closing) {
      this.stream.once("finish", () => cb());
      if (!this.closing) {
        this.closing = true;
        this.stream.end();
      }
    } else {
      this.once("ready", () => this.endStream(cb));
      this.once("failed", () => this.endStream(cb));
    }
  }

  _checkStats(cb) {
    fs.stat(this.file, (err, stats) => {
      if (err != null) { return cb(err); }
      if (!stats.isFile()) { return cb(new Error("Path is not a file: " + this.file)); }

      this.size = stats.size;
      if (this.maxAge != null) {
        let bTime = stats.birthtime.getTime();
        if (bTime === 0) { bTime = new Date().getTime(); }
        this.maxDate = bTime + this.maxAge;
      }
      cb();
    });
  }

  _createStream(cb) {
    cb = once(cb);
    let stream = this.stream = fs.createWriteStream(this.file, {flags: "a", defaultEncoding: this.encoding});
    stream.once("finish", () => this.finished = true);
    stream.once("open", () => cb());
    stream.once("error", cb);
  }

}

/*===================================================== Exports  =====================================================*/

export default FileRotateScope;
