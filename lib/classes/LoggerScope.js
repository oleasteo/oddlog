import EventEmitter from "events";
import {isAnySetAndTest as isEnvMatching} from "../services/env";
import {fastStringEscape} from "../utils/misc";

/*===================================================== Classes  =====================================================*/

/**
 * Instances are to be shared between Logger instances and their children except a child demands to use a dedicated
 * LoggerScope. The LoggerScope requires transports to notify about the status of asynchronous write operation. This
 * allows to listen for related events.
 *
 * The following Events are emitted:
 *
 *  * "drain" - Whenever all asynchronous messages have been flushed.
 *  * "close" - When the scope is getting closed.
 *  * "end" - When all messages have been flushed after the scope has been closed.
 *
 * @class
 * @see logger.createLogger
 */
class LoggerScope extends EventEmitter {

  /**
   * Creates a new LoggerScope.
   *
   * @param {String} name The name of the logger scope.
   * @param {?Boolean} [logSource] Whether to use source location logging for loggers of this scope. If not set, it
   *        defaults to `false` iff no environment variable is matching.
   * @param {LoggerScope} [parent] The parent LoggerScope.
   * @constructor
   * @private
   */
  constructor(name, logSource, parent) {
    super();
    this.name = name;
    this.logSource = logSource == null ? isEnvMatching(name) : logSource;
    this.closed = false;
    this.busyWrites = 0;
    this._prefix = fastStringEscape(name) + "\",";
    this._parent = parent;
    this._throw = false;
    if (parent != null) {
      this._parentOnClose = () => this._close(!parent._throw);
      parent.on("close", this._parentOnClose); // prevents garbage collection of this until close() is called
    }
  }

  /**
   * To be called when a Transport has started an asynchronous message delivery.
   */
  down() {
    this.busyWrites++;
    if (this._parent != null) { this._parent.down(); }
  }

  /**
   * To be called when a Transport has finished an asynchronous message delivery. It is also supposed to be called
   * during a process-task that is not triggered by the user application (e.g. process.nextTick).
   */
  up() {
    if (!--this.busyWrites) { this.emit("drain"); }
    if (this._parent != null) { this._parent.up(); }
  }

  /**
   * Alias for {@link LoggerScope#up} for multiple message deliveries.
   *
   * @param {Number} num The amount of message deliveries that have been finished.
   */
  upTimes(num) {
    if (!(this.busyWrites -= num)) { this.emit("drain"); }
    if (this._parent != null) { this._parent.upTimes(num); }
  }

  /**
   * Closes this logger scope and all children.
   *
   * @param {Boolean} failSilent Whether to silently ignore future incoming logs.
   * @private
   */
  _close(failSilent) {
    this.closed = true;
    this._throw = !failSilent;
    if (this._parent != null) {
      this._parent.removeListener("close", this._parentOnClose); // release this from parent for garbage collection
    }
    this.emit("close");
    let asyncCb = () => this.emit("end");
    if (this.busyWrites) { this.once("drain", () => process.nextTick(asyncCb)); } else { process.nextTick(asyncCb); }
  }

}

/*===================================================== Exports  =====================================================*/

export default LoggerScope;

/**
 * The separation between logger scope names.
 *
 * @type {String}
 */
export const SCOPE_NAME_SEPARATOR = ":";
