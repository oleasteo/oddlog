# oddlog - Object driven data logging

[![License](https://img.shields.io/npm/l/oddlog.svg)](LICENSE)
[![Version](https://img.shields.io/npm/v/oddlog.svg)](https://www.npmjs.com/package/oddlog)
[![build status](https://gitlab.com/frissdiegurke/oddlog/badges/master/build.svg)](https://gitlab.com/frissdiegurke/oddlog/commits/master)
[![coverage report](https://gitlab.com/frissdiegurke/oddlog/badges/master/coverage.svg)](https://gitlab.com/frissdiegurke/oddlog/commits/master)
[![Downloads](https://img.shields.io/npm/dm/oddlog.svg)](https://www.npmjs.com/package/oddlog)

oddlog is a high-performance payload focused logging library for node.js.

*Being restricted to strings for logging sucks. So we use arbitrary JSON objects instead.*

[![Preview 01](https://gitlab.com/frissdiegurke/oddlog/raw/master/assets/readme-01.png)](https://gitlab.com/frissdiegurke/oddlog/raw/master/assets/readme-01.png)

```javascript
const oddlog = require("oddlog");
const log = oddlog.createLogger("my-app"), child = log.child({user: "root"});
log
  .handleUncaughtExceptions()
  .info({random: 42}, "Application startup");
child
  .warn({method: "terminate"}, "Procedure initiated");

throw new Error("Nah, nothing failed");
```

## References

* [Website](https://frissdiegurke.gitlab.io/oddlog)
* [User Guide](https://frissdiegurke.gitlab.io/oddlog/guide)
* [Documentation](https://frissdiegurke.gitlab.io/oddlog/documentation)
* [Source code](https://gitlab.com/frissdiegurke/oddlog)

## Installation

```bash
npm install --save oddlog
```

## Features

 * Payload inheritance
 * Stateless and stateful logging APIs
 * Multi-transport logging (one log, multiple destinations)
 * Source code location logging (for development, not production)
 * Payload attribute transformation (e.g. `req`, `res`, `err`)
 * Termination methods (ensure all logs have been transferred)
 * Suited for libraries (via option flag `shy`)
 * CLI for (pretty) output reformatting and filtering of log records
 * Threshold logging (cache log records until high severity is logged)

## Command-line interface

The CLI `oddlog-cli` is a command-line tool to reformat, prettify and filter message records. It accepts input from
`stdin` and outputs on `stdout`; Thus it suits well for any common operations.

```bash
node my-application | oddlog        # simply prettify the output of my-application
node my-application | oddlog -vv    # increased verbosity to show payload as well

cat logs/app.log | oddlog -vvl info # read (pretty) log records from logs/app.log that are INFO or above

tail -f logs/app.log | oddlog       # pretty print latest and future log records within logs/app.log
                                    # this is the recommended way of checking live logs of a running application; it
                                    # allows you to change the filters, etc. without interrupting the process itself.
```

For further information take a look into `oddlog --help` or check out the
[oddlog-cli repository](https://gitlab.com/frissdiegurke/oddlog-cli).

## License

The source code and all related examples, tests and benchmarks are licensed under
[MIT](https://gitlab.com/frissdiegurke/oddlog/blob/master/LICENSE). If you don't agree to the license, you may not
contribute to the project. Feel free to fork and maintain your custom build thought.

## Thanks

Many thanks also to everyone who supports this project by usage, contribution and/or spreading the word!

All contributors may add themselves to the `contributors` field within the *package.json* regardless of the amount of
changes; however *I* won't do it for them.

Since this project is inspired by [bunyan](https://www.npmjs.com/package/bunyan), at this point a big *Thanks!* to its
author.

## Donations

Donations (dt.: *Schenkungen*) to the author are accepted via [PayPal](https://www.paypal.me/frissdiegurke) and
[Patreon](https://www.patreon.com/frissdiegurke).

## Related projects

 * [bunyan](https://github.com/trentm/node-bunyan) - The main inspiration for this project.
 * [winston](https://github.com/winstonjs/winston) - Some inspiration came from winston as well.
