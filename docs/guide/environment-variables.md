# Environment variables

There are two environment variables that affect oddlog:

* `DEBUG` - Decrease the minimum level of matching loggers to `oddlog.DEBUG`.
* `TRACE` - Decrease the minimum level of matching loggers to `oddlog.TRACE`.

Both accept the same type of patterns and
are parsed by [ns-matcher](https://gitlab.com/frissdiegurke/ns-matcher#pattern). Thus values might look like one of
these:

```bash
# Start the app for debugging everything
env DEBUG='**' node app.js
# Start the app for debugging everything within my-app but the http logger
env DEBUG='my-app:**,!my-app:http' node app.js
# Start the app for debugging of my-app and tracing of my-app:http
env DEBUG='my-app:**' TRACE='my-app:http' node app.js
```

By default the level of a transport, if not explicitly set, falls back to `oddlog.DEBUG`. If at least one of the
environment variables is set, the fallback level of transports, whose logger does not match either variable, is raised
to `oddlog.INFO` in order to avoid noise during debugging. This has no effect, if the fallback level is already at
`oddlog.WARN` due to the `shy` option.
