---
prev: false
---

# Introduction

## Preamble

Do not get scared away by the length of this guide; you can just skim over the most of it and probably skip some parts.
I've done my best to clearly outline the features and different components of oddlog in this guide. Still there is
probably a whole lot to improve. I'm glad about any merge requests for improvements of this guide! Have a nice day!

## What is oddlog?

Oddlog is a sophisticated and performance-optimized logging library for node.js. It is suited for libraries as well as
applications. The main concept is to not only log strings but to log whole objects in addition; this provides much more
information for debugging and monitoring purposes.

## When to use?

In many applications a simple string based logging approach does not provide enough information to debug any errors
post-mortem. For those applications you'll want to use payload-based logging instead. Oddlog makes this kind of logging
very simple and provides advanced mechanisms for debugging. Take a look at the pros & cons below to decide whether to
use oddlog for your application.

For libraries that are large enough for logging to make sense at all, oddlog is a good match as it provides a simple
flag (`shy`) to raise threshold levels to `WARN` and thus replace logging methods below with no-operations. Debugging
with the `shy`-flag enabled is as easy as setting up an [environment variable](./environment-variables.md).

## Alternatives

### Context

Since we are only aware of [bunyan](https://www.npmjs.com/package/bunyan) (version 1.x) and
[pino](https://www.npmjs.com/package/pino) (version 4.x) as comparable contestants to oddlog, we will take direct
comparisons and speak out recommendations based on those.

### Bunyan is superseded

Both oddlog and pino are meant to supersede bunyan of that they were inspired by. So despite being an innovative logger
for javascript, we consider bunyan obsolete as it does not provide any value over oddlog or pino (in the context of
node.js). The last reason to use bunyan in favor of the others might be available transport plugins. In this case,
please file an [issue](https://gitlab.com/frissdiegurke/oddlog/issues) so the demand is noticed.

This logging library has been highly inspired by bunyan. A big **thank you** to its author!

### Pino vs oddlog

Our [benchmarks](./benchmarks.md) show that pino and oddlog perform comparable well; one better in some cases, the other
one in other cases. There is no clear winner here.

Notice that the benchmarks are only utilizing stream transports (except for oddlog:message-cache). For heavier
transports (e.g. file-rotation), pino would be better as pinos transports operate in their own processes.

Now things are getting interesting. Pino utilizes out-of-process transports by design. This is a great approach except
for one case: You do not have advanced log *filters* in-process. One of oddlogs star transports is the
[TriggerTransport](./transports.md#triggertransport) that does not forward logs unless some log with a high level (e.g.
`WARN` or `ERROR`) is emitted. If such a log is emitted, the transport flushes all cached logs, providing a detailed
stack of logs that lead to the high-severity log. As to our experience this stack can be very helpful in speeding things
up for debugging. In addition it ensures that no logs are processed that don't need to be flushed at all; this results
in high performance benefits (factor `10` upwards) for those logs at the cost of some memory.
So if you're planning to use this transport type, oddlog is probably the right choice. If not, consider out-of-process
transports with pino.
