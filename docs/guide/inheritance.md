# Inheritance

## Children

In oddlog, re-use of the same payload data is implemented via inheritance. The `Logger#child` method creates a new child
logger that carries its own copy of the parent loggers payload. `Logger#child` can be called with additional payload to
be merged into the payload of the new child. Similar to one-time payload passing, `null` would create a child without
any attached payload.

```javascript
const myChildLogger = logger.child(true, {additional: "payload"});

myChildLogger.info({it: "works!"}, "yay");  // payload: {additional: "payload", it: "works!"}}

myChildLogger
  .child(true, {test: 42})
  .info("pew");  // payload: {additional: "payload", test: 42}
  
myChildLogger
  .child(null)
  .info({hello: "world"}, "pew");  // payload: {hello: "world"}
```

## Express.js middleware

For a common use-case oddlog provides some semantic sugar:

```javascript
// add middleware to express.js router
router.use(logger.mwExpress());
```

The [`Logger#mwExpress`](../documentation/logger.md#mwexpress) method accepts arguments similar to `Logger#child`, but
the `payload` argument may be a function that gets `req` and `res` passed. It will create a child logger for each
incoming request and attach it to the request object as `req.log`. If no payload is explicitly passed, `{req, res}` will
be used as child payload.
