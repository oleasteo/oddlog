# Plugins

## Using plugins

Oddlog provides a `oddlog.mixin()` function to attach arbitrary plugins.

Use it like this:

```javascript
import * as myPlugin from "oddlog-plugin-xyz";

oddlog.mixin(oddlog, [
  myPlugin,
  // ...
]);
```

## Writing plugins

If you write a plugin, you should either export an init function, or export an object that has `init: function(oddlog)`.
Within that function, modify oddlog as you wish.

Naming convention: Please name your plugin `oddlog-{transport|transform|plugin}-...` according to its type. Do not
create further types; the `oddlog-...` namespace should be reserved for clients such as `oddlog-cli`.
