---
prev: false
next: false
---

# Transports

## Types

### StreamTransport

*Inherits:* [BasicTransport](#basictransport)

| Option | Type | Description |
| --- | --- | --- |
| `stream` | `stream.Writable` | default: `process.stdout` - The stream to write to. |
| `closeStream` | `Boolean` | default: `false` - Whether to close the stream when the logger scope gets closed. |
| `format` | `String|{String}Function<log>` | default: `"raw"` - The record format to use or a formatting function. Format options: `"raw"`, `"simple"`. |
| `separator` | `String` | default: `os.EOL` - String to append to every log record. |
| `encoding` | `?String` | If set, `stream.setDefaultEncoding(encoding)` is called. |

With `shy` set the `format` defaults to `"simple"` (does not apply to [FileTransport](#filetransport)).

### FileTransport

*Inherits:* [StreamTransport](#streamtransport), [BasicTransport](#basictransport)

| Option | Type | Description |
| --- | --- | --- |
| `path` | `String` | The file path to write to. |

Does not accept inherited options: `stream` (created by `path`) and `closeStream` (always `true`).

### FileRotateTransport

*Inherits:* [DeferredTransport](#deferredtransport), [BasicTransport](#basictransport)

Gzip compression is applied during first rotation.

Only during rotations, incoming logs are deferred and kept within an internal cache.

| Option | Type | Description |
| --- | --- | --- |
| `path` | `String` | The path of the main file to log to (no rotation suffix). |
| `separator` | `String` | default: `os.EOL` - |
| `encoding` | `String` | default: `"utf8"` - |
| `size` | `Number|String` | If set, it determines the maximum file size (before rotation/compression). Accepts all common size descriptors or bytes as number. |
| `interval` | `Number|String` | If set, it determines the maximum time-span between rotations. Accepts all common time descriptors or milliseconds as number. |
| `maxFiles` | `Number` | If set, only up to the specified amount of files are stored; older logs will be removed. |
| `compress` | `Boolean|Object` | default: `false` - Whether to use gzip for compression of rotated files. An object may be passed to set [compression options](https://nodejs.org/api/zlib.html#zlib_class_options). |

**Events** are emitted on the logger with `transport:file-rotate:` prefix as following:

| Name | Arguments | Trigger |
| --- | --- | --- |
| `cork` | `{FileRotateScope} scope`, `{?FileRotateScope} oldScope` | Whenever the FileRotateScope gets changed. |
| `uncork` | `{?Error} error` | Whenever the change of FileRotateScope is completed. |
| `rotation:start` | | Whenever a file rotation gets initiated. |
| `rotation:end` | `{?Error} error` | Whenever a file rotation has ended. |

### CapTransport

*Inherits:* [NestedTransport](#nestedtransport), [BasicTransport](#basictransport)

| Option | Type | Description |
| --- | --- | --- |
| `cap` | `Number|String` | default: `oddlog.FATAL` - The maximum level of logs allowed to be passed to nested transports. |

### SplitTransport

*Inherits:* [NestedTransport](#nestedtransport), [BasicTransport](#basictransport)

| Option | Type | Description |
| --- | --- | --- |
| `split` | `(?Number|String)[]` | The level thresholds. Each log is only passed to a single nested `transports` according to the split-points. Transport index `i` is used for `split[i-1] <= level < split[i]` with `split[-1] := 0`. |

### TriggerTransport

*Inherits:* [NestedTransport](#nestedtransport), [DeferredTransport](#deferredtransport), [BasicTransport](#basictransport)

| Option | Type | Description |
| --- | --- | --- |
| `threshold` | `Number|String` | default: `oddlog.ERROR` - The minimum level to trigger cache flooding. |
| `guard` | `Boolean` | default: `true` - Whether to wrap the cached logs with guard logs (`--- start/end cache ---`) on flooding. |
| `limit` | `Number` | default: `50` - The amount of logs to keep in cache (for RayCache, `window` additional logs can be present within cache - the discard-window). |
| `window` | `Boolean|Number` | default: `limit >= 4096` - If `false`, use a **RingCache**. If number (or `true` which is interpreted as `max(5, limit/20)`), use a **RayCache** with the specified size as discard-window. |
| `replicate` | `Boolean|Number` | default: `false` - If `true`, create dedicated caches for children. If a number is provided, create dedicated caches for children up of that depth. |
| `cleanUp` | `Boolean` | default: `true` - Whether to clear the cache after flooding. |

If a **RayCache** is used (see `window` option), the following additional options are available:

| Option | Type | Description |
| --- | --- | --- |
| `silentWindow` | `Boolean` | default: `false` - Whether to skip the logs within discard-window from being flooded. This ensures that at most `limit + 1` logs are flooded at once. |
| `malloc` | `Number` | optional. If set, specifies the size of the initial cache array. |

If a **RingCache** is used (see `window` option), the following additional options are available:

| Option | Type | Description |
| --- | --- | --- |
| `freeUp` | `Boolean` | default: `cleanUp` - Whether to free up log references for garbage collection on cache clear after flooding. Disabling gives a small (`~5%`) performance boost. |

**Events** are emitted on the logger with `transport:trigger:` prefix as following:

| Name | Arguments | Trigger |
| --- | --- | --- |
| `flooding:start` | `{Number} id`, `{Object} payload` | Whenever a flooding gets started. |
| `flooding:end` | `{Number} id`, `{Object} payload` | Whenever a flooding got completed. |

#### RayCache

This cache type has a low memory footprint but bad performance.

The ray cache structure appends incoming logs to an array. The array size grows as more logs are appended. When
`limit + window` size is reached, the first `window` logs are spliced from the array.

#### RingCache

This cache type has great performance but a high (pre-allocated array of `limit` size) memory footprint.

The ring cache structure pre-allocates an array only once for the maximum needed size. It saves two indices, one for
start and one for end position. On incoming logs the log is put at the end position and it is increased. The array is
used as a ring and on trigger all logs between start (inclusive) and end (exclusive) are flooded.

### MemoryTransport

*Inherits:* [DeferredTransport](#deferredtransport), [BasicTransport](#basictransport)

| Option | Type | Description |
| --- | --- | --- |
| `store` | `{push:Function(Message)}` | default: `[]` - The store to push logs to. |

### VoidTransport

Ignores all options. When used, it is a singleton instance. Does ignore everything.

## Base classes

### BasicTransport

*Inherited by:* All built-in transports (except for [VoidTransport](#voidtransport)).

A class to provide most basic transport options.

| Option | Type | Description |
| --- | --- | --- |
| `shy` | `Boolean` | default: `logger.shy` - Whether to use sane defaults for libraries instead of applications. |
| `level` | `Number|String` | default: `shy ? oddlog.WARN : oddlog.DEBUG` - The minimum level of logs to process. |

*Note:* The `level` option is ignored if a matching [environment variable](../guide/environment-variables.md) is available.

### DeferredTransport

*Inherited by:* [FileRotateTransport](#filerotatetransport), [TriggerTransport](#triggertransport)

A class to be inherited by transports that may not deliver their logs immediately (e.g. internal cache). It provides
options to tweak the laziness in such cases.

| Option | Type | Description |
| --- | --- | --- |
| `immediateFormatting` | `Boolean` | default: `false` - Whether to immediately apply log message formatting. |
| `immediateOwnership` | `Boolean` | default: `true` - Whether to immediately shallow copy payloads that oddlog may not modify (see [performance considerations](../guide/performance-considerations.md#specify-permissions-for-oddlog). |
| `immediateTransform` | `Boolean` | default: `false` - Whether to immediately merge payloads (implies `immediateOwnership`). |
| `immediateStringify` | `Boolean` | default: `false` - Whether to immediately create the raw message string (implies `immediateTransform`). |
| `immediateFreeze` | `Boolean` | default: `false` - Whether to immediately deep clone the payload; do not use unless you know what you are doing (significant performance impacts). |

### NestedTransport

*Inherited by:* [CapTransport](#captransport), [TriggerTransport](#triggertransport)

A class to be inherited by transports that have nested transports to forward messages to.

| Option | Type | Description |
| --- | --- | --- |
| `transports` | `(?Object)[]` | A list of nested transport definitions. |

## Custom Transports

Custom transports must implement the following methods:

* `write({Message} log)` - Called on each log action.
* `{Number} minLevel()` - Should return the minimum log level that is processed.
* `{Transport} child({boolean} hasChildScope)` - Called when a child logger is created. `hasChildScope` specifies
   whether the child logger has a dedicated logger scope. This method must return a Transport (may be the identity).

In addition the following optional method can be implemented:

* `attach({Logger} logger)` - Called when the transport is being attached to a logger.

::: tip
When `Transport#attach()` is implemented, it probably makes no sense to return the identity within `Transport#child()`.
:::

Also keep in mind that you may want to inherit from built-in classes.

You may use a custom transport instance via `{instance: myTransport}` in place of any transport definition. However it
is preferred to publish those as [plugins](../guide/plugins.md) instead if they are re-usable by others.
