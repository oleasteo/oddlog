---
prev: false
next: false
---

# Class: LoggerScope

A class used to internally share information between parent loggers and their children. Child logger scopes keep a
reference to their parent logger scope.

## Events

| Name | Arguments | Trigger |
| --- | --- | --- |
| `drain` | | Whenever the last asynchronous log transportation (on this scope and all children) has finished. |
| `close` | | When the logger scope and its children are to be closed. |
| `end` | | When the logger scope has been closed and all logs have been flushed to their destinations. |

## Attributes

### name

The name of the logger scope as provided within
[`createLogger`](./README.md#createlogger)/[`defineLogger`](./README.md#definelogger).

**Type:** `String`

### logSource

Whether source location logging is enabled.

**Type:** `Boolean`

### closed

Whether the logger scope has been closed. No further logs are accepted.

**Type:** `Boolean`

### busyWrites

The amount of active asynchronous transport operations (see methods); sum of all operations on child logger scopes as
well as the own ones.

**Type:** `Number`, non-negative

## Methods

### down

Signalizes the logger scope that some log has begun being transported to their destination via an asynchronous
operation. As long as any such operation is in progress, the logger scope will defer getting closed.

**Returns:** `undefined`

### up

Signalizes the logger scope that an asynchronous transport operation has finished.

**Returns:** `undefined`

### upTimes

Alias for calling `LoggerScope#up()` `num` times. Available for performance gains (do not check for being drained after
every single decrement).

| Parameter | Type | Description |
| --- | --- | --- |
| `num` | `Number` | The amount of asynchronous transport operations that have finished. |

**Returns:** `undefined`
