---
prev: false
next: false
---

# Exports

## Default

The default export is [`getLogger`](#getlogger).

Within transpiled *dist/* version, the default export is available as `oddlog.default`.

## Constants

### stdTransformer

See [transformers](./transformers.md).

### levels

The exported attribute `levels` has a key-value mapping from *lowercase* level name to the number value. In addition an
*uppercase* attribute for each level is exported directly as well:

```
SILENT = 0
TRACE = 1
DEBUG = 2
VERBOSE = 3
INFO = 4
WARN = 5
ERROR = 6
FATAL = 7
```

## Functions

### createLogger

Creates a new logger based on passed options.

| Parameter | Type | Description |
| --- | --- | --- |
| `name` | `String` | The name of the logger (also used as store key for `getLogger`). |
| `options` | `Object` | optional. Any options for the logger (see below). |
| `payload` | `?Object` | optional. Payload for all logs of this logger and its children. |

The `options` parameter may provide the following attributes:

| Option | Type | Description |
| --- | --- | --- |
| `transports` | `(?Object)[]` | default: `[{type:"stream"}]` - A list of options to derive transports of. Each entry creates one Transport. |
| `transformer` | `!Object` | default: [`stdTransformer`](./transformers.md) - Transformer mapping to apply to log payloads. |
| `ownPayload` | `Boolean` | default: `false` - Whether the given `payload` may be modified by the Logger. |
| `ownChildPayloads` | `Boolean` | default: `false` - Set the default for `ownPayload` of child and logging methods. |
| `singleTransform` | `Boolean`| default: `true` - Whether transformations shall only be executed at log level. If set to `false` the inherited payloads will be transformed before caching. This can speed up the logging (especially for computational complex transformers), but will freeze the value of inherited payload for all its logs.
| `meta` | `?Object` | default: `[platform,host,pid]` - Static meta information to add to logs. The default values are `os.release()`, `os.hostname()` and `process.pid`. Set to `null` to disable meta information. |
| `typeKey` | `?String` | default: `"_type"` - The key to use for a payloads (nested) type identifications. Supported types by the log reader can be specified within any payload data at this key. |
| `logSource` | `Boolean` | default: `false` - Whether to add the source code location of the logging method caller to log records. |
| `messageFormatter` | `?{String}Function<String,?Object|String...>` | default: `util.format` - The formatting function to use for log messages. |
| `level` | `Number,String` | default: `INFO` - The initial level for the stateful logging methods. |
| `shy` | `Boolean` | default: `false` - If `true`, transport fallback level will be `WARN` instead of `DEBUG` and stream transport fallback format will be `"simple"` instead of `"raw"`. |

**Returns:** A new [`Logger`](#logger) instance.

### defineLogger

Creates a new logger based on passed options and stores it within the logger store.

The arguments can be either of the two signatures below:

| Parameter | Type | Description |
| --- | --- | --- |
| `name` | `String` | The name of the logger (also used as store key for `getLogger`). |
| `options` | `Object` | optional. Any options for the logger (see [`createLogger`](./README.md#createlogger) above). |
| `payload` | `?Object` | optional. Payload for all logs of this logger and its children. |

| Parameter | Type | Description |
| --- | --- | --- |
| `key` | `String` | default: `logger.loggerScope.name` - The store key for `getLogger`. |
| `logger` | `Logger` | The logger to store; to be retrieved by `getLogger(key)`. |

**Returns:** The [`Logger`](#logger) that is being defined.

### getLogger

Retrieves a logger previously defined via [`defineLogger`](#definelogger).

| Parameter | Type | Description |
| --- | --- | --- |
| `name` | `String` | The name of the logger to retrieve from store. |
| | `...` | If additional arguments are provided, a child logger will be created and returned; additional arguments are passed to [`Logger#child`](./logger.md#child).

**Returns:** The [`Logger`](#logger) stored at the provided name.

### createTransport

| Parameter | Type | Description |
| --- | --- | --- |
| `options` | `Object` | The options for the Transport. If a `type` attribute is set, it will be used to determine the Transport type. |

**Returns:** The new [`Transport`](#transports) instance.

### defineType

| Parameter | Type | Description |
| --- | --- | --- |
| `key` | `String` | The identifier of the type (as to be used within transport definition `type` attribute). |
| `createFn` | `{Transport} Function(options, createTransport)` | The creation function for new transports of this type. Is called without `new` keyword and gets passed the `options` as well as a function that creates transports from options. |
| `overwrite` | `Boolean` | default: `false` - Whether to overwrite already existing transport type with the same `key`. |

**Returns:** `undefined`

### eachScopeOnce

| Parameter | Type | Description |
| --- | --- | --- |
| `loggers` | `Logger[]` | The loggers whose logger-scopes to listen on. Note: Their logger-scopes are assumed to be disjoint. |
| `event` | `String` | The event to listen for. |
| `cb` | `Function<>` | The event callback. Called once by each logger-scope. |

**Returns:** `undefined`

### closeAll

| Parameter | Type | Description |
| --- | --- | --- |
| `failSilent` | `Boolean` | default: `true` - Whether to silently ignore future incoming logs; throw Error otherwise. |
| `loggers` | `Logger[]` | The loggers to close. |
| `cb` | `Function<>` | Gets called when all logs have been flushed. |

**Returns:** `undefined`

### mixin

| Parameter | Type | Description |
| --- | --- | --- |
| `oddlog` | exports of this package | The oddlog package for which the plugin(s) should be initialized. |
| `plugins` | `Function|{init:Function}|Array` | The plugin(s) to mix into this package. |

**Returns:** `undefined`

## Utilities

### assign

| Parameter | Type | Description |
| --- | --- | --- |
| `target` | `Object` | The target. |
| `sources` | `Object...` | The sources. |

**Returns:** `target`

### clone

| Parameter | Type | Description |
| --- | --- | --- |
| `obj` | `?Object|*` | The source. |

**Returns:** `?Object|*` - The clone.

### cloneDeep,

| Parameter | Type | Description |
| --- | --- | --- |
| `obj` | `?Object|*` | The source. |

**Returns:** `?Object|*` - The clone.

## Classes

* [Logger](./logger.md)
* [Log](./log.md)
* [LoggerScope](./logger-scope.md)

### Transports

The following transport classes are exported:

* [MemoryTransport](./transports.md#memorytransport)
* [StreamTransport](./transports.md#streamtransport)
* [FileTransport](./transports.md#filetransport)
* [FileRotateTransport](./transports.md#filerotatetransport)
* [CapTransport](./transports.md#captransport)
* [SplitTransport](./transports.md#splittransport)
* [TriggerTransport](./transports.md#triggertransport)
* [VoidTransport](./transports.md#voidtransport)
