---
prev: false
next: false
---

# Class: Logger

The main class of this library. Provides methods for inheritance and logging.

## Events

| Name | Arguments | Trigger |
| --- | --- | --- |
| `error` | `{Error} error` | An error has occurred on any underlying data (e.g. streams). |
| `log` | `{Log} log` | Whenever a logging method got called. |
| `child` | `{Logger} child` | Whenever a child logger got created. |

Some [transport types](./transports.md#types) emit transport specific events on the logger they are attached to as well.

## Attributes

### loggerScope

The logger scope of this logger.

**Type:** [`LoggerScope`](./logger-scope.md)

### transformer

The transformers to apply to any payload of logs from this logger.

**Type:** `?Object`

### transports

The transports to forward logs to.

**Type:** [`Transport[]`](./transports.md)

### ownChildPayloads

The default value whether payload ownership can be assumed to be given to oddlog for [logging methods](#logging-methods)
and [child creation](#child).

**Type:** `Boolean`

### messageFormatter

The function to call for primary log message formatting.

**Type:** `{String} Function<String, ?Object|String...>`

## Methods

TODO sort methods by similarity

### Logging methods

#### Stateless

*For each [severity level](./under-the-hood.md#logging-levels), a method is provided as `Logger#<level>` (e.g.
`Logger#info`) to create a log with that level.*

| Parameter | Type | Description |
| --- | --- | --- |
| `ownPayload` | `Boolean` | default: `logger.ownChildPayloads` - Whether to permit oddlog to modify the provided payload. |
| `payload` | `?Object` | optional. The log payload. If set to `null`, no payload is inherited, otherwise it's merged with inherited payload. |
| `message` | `String` | optional. The primary message. Required, if `formatValues` are provided. |
| `formatValues` | `String...|Object` | optional. The format value(s) to apply to the primary message. See [`util.format()`](https://nodejs.org/api/util.html#util_util_format_format_args) |

**Returns:** `this`

**Examples:**

```
logger.info();         // log without log-specific payload and message
logger.info({});       // log with custom payload
logger.info(true, {}); // log with custom payload that is allowed to be modified by the logger (for performance
                       // gains)
// The following calls also accept additional formatting parameters.
logger.info("");           // log with primary (formatted) message text
logger.info({}, "");       // log with primary (formatted) message text and custom payload
logger.info(true, {}, ""); // log with primary (formatted) message text and custom payload that is allowed to be
                           // modified by the logger (for performance gains)
```

#### Stateful

##### setLevel

Sets the stateful level for calls of the [`Logger#log`](#log) method.
For each level an alias is provided as well as `Logger#set<Level>` (e.g. `Logger#setInfo`) without arguments.

| Parameter | Type | Description |
| --- | --- | --- |
| `level` | `Number|String` | The level to use for further logs. |

**Returns:** `this`

##### log

Create a new log with the current level. Same parameters as for the [stateless methods](#stateless).

**Returns:** `this`

##### done

Resets the stateful level to the default level (can be specified within
[`createLogger`](./README.md#createlogger)/[`defineLogger`](./README.md#definelogger)).

**Returns:** `this`

##### getLevel

**Returns:** `Number` - The current stateful level.

### Guarding methods

*For each [severity level](./under-the-hood.md#logging-levels), a method is provided as `Logger#is<Level>` (e.g.
`Logger#isInfo`) to check whether any transport might be interested in such logs.*

**Returns:** `Boolean` - Whether (purely based on the level) any transport might be interested in logs of this level.

### child

Creates a child logger that inherits payload of this logger and uses the same options (such as transports).

| Parameter | Type | Description |
| --- | --- | --- |
| `ownPayload` | `Boolean` | default: `this.ownChildPayloads` - Whether the payload ownership is transferred to oddlog. |
| `ownChildPayloads` | `Boolean` | default: `this.ownChildPayloads` - Fallback value for `ownPayload` parameters of methods of the child logger. |
| `childScope` | `Boolean|String` | default: `false` - If set, a child [logger scope](./logger-scope.md) will be created for the child logger. If a string is passed, it will be appended to the child logger scopes name. |
| `payload` | `?Object` | optional. Payload of the child logger. Unless `null` is passed, the parent loggers payload will be merged into this. |

**Returns:** `Logger` - The child logger.

**Examples:**

```
logger.child();                // a new logging child; E.g. to create dedicated threshold cache.
logger.child({});              // a new logging child with additional payload (merged into parent payload)
logger.child(true, {});        // a new logging child with additional payload that is allowed to be modified by the
                               // logger (for performance gains)
logger.child(true, true, {});  // a new logging child with additional payload that is allowed to be modified by the
                               // logger (for performance gains); Set `ownChildPayloads` for the child to `true` as well
logger.child(true, "sub", {}); // a new logging child with additional payload that is allowed to be modified by the
                               // logger (for performance gains); The child uses a dedicated logger scope with a
                               // suffixed name
```

### close

Closes the logger scope of this logger. This method must not be called on logger children (if they do not have a dedicated logger scope).

| Parameter | Type | Description |
| --- | --- | --- |
| `failSilent` | `Boolean` | default: `true` - Whether to silently ignore future incoming messages. If set to `false` an Error will be thrown on further logging method calls. |
| `cb` | `Function<>` | optional. The callback. |

**Returns:** `this`

**Examples:**

```
logger.close();          // close the loggers scope
logger.close(false);     // close the loggers scope and throw an error on further attempts to log
logger.close(cb);        // close the loggers scope and call the callback when all messages have been flushed
logger.close(false, cb); // the two above combined

oddlog.closeAll(false, [log1, log2, ...], cb); // similar to methods above for multiple loggers

logger.exit(code);       // alias for `logger.close(() => process.exit(code));`
```

### exit

Closes the logger scope of this logger and calls `process.exit` when done.

| Parameter | Type | Description |
| --- | --- | --- |
| `code` | `Number` | optional. The exit code to pass to `process.exit`. |

**Returns:** `this`

### handleUncaughtExceptions

Registers an event handler to the `uncaughtException` event on `process`. The event handler gets removed when [`Logger#close`](#loggerclose) gets called on the logger or any ancestor.

| Parameter | Type | Description |
| --- | --- | --- |
| `level` | `Number|String` | default: `oddlog.FATAL` - The log level to use for generated records. |
| `exit` | `Boolean|Number|{Boolean|Number}Function<error[,cb]>` | default: `true` - Whether to exit when occuring. If a `Number` is passed, this will be used as exit code. |

**Returns:** `this`

### handleUnhandledRejections

Registers an event handler to the `unhandledRejection` event on `process`. The event handler gets removed when [`Logger#close`](#loggerclose) gets called on the logger or any ancestor.

| Parameter | Type | Description |
| --- | --- | --- |
| `level` | `Number|String` | default: `oddlog.ERROR` - The log level to use for generated records. |
| `exit` | `Boolean|Number|{Boolean|Number}Function<error[,cb]>` | default: `false` - Whether to exit when occuring. If a `Number` is passed, this will be used as exit code. |

**Returns:** `this`

### handleWarnings

Registers an event handler to the `warning` event on `process`. The event handler gets removed when [`Logger#close`](#loggerclose) gets called on the logger or any ancestor.

| Parameter | Type | Description |
| --- | --- | --- |
| `level` | `Number|String` | default: `oddlog.WARN` - The log level to use for generated records. |
| `exit` | `Boolean|Number|{Boolean|Number}Function<warning[,cb]` | default: `false` - Whether to exit when occuring. If a `Number` is passed, this will be used as exit code. |

**Returns:** `this`

### handleProcessEvents

Registers an event handler to the given event on `process` that logs logs the event data. The event handler gets removed when [`Logger#close`](#loggerclose) gets called on the logger or any ancestor.

| Parameter | Type | Description |
| --- | --- | --- |
| `eventName` | `String|String[]` | The name of the event(s) to listen to. |
| `message` | `String|{String}Function<event,logger>` | The primary message of the log to generate. |
| `level` | `Number|String` | The log level to use for generated records. |
| `exit` | `Boolean|Number|{Boolean|Number}Function<error[,cb]>` | default: `false` - Whether to exit when occuring. If a `Number` is passed, this will be used as exit code. |
| `key` | `?Object|String` | optional. The payload key to use for the event data - if any. If an object is given, it will be used as payload instead. If omitted, the event data will be used as payload. |

**Returns:** `this`

### addTransport

Creates a new transport and attaches it to this logger.

| Parameter | Type | Description |
| --- | --- | --- |
| `transportOptions` | `Object` | Same options as [`oddlog.createTransport`](./README.md#createtransport) accepts. |

**Returns:** `this`

### addTransports

Creates new transports and attaches them to this logger.

| Parameter | Type | Description |
| --- | --- | --- |
| `transportOptionsList` | `Object[]` | A list of options as accepted by [`oddlog.createTransport`](./README.md#createtransport). |

**Returns:** `this`

### mergePayload

Merges additional payload into this loggers payload.

| Parameter | Type | Description |
| --- | --- | --- |
| `ownPayload` | `Boolean` | default: `ownChildPayloads` - Whether the payload ownership is transferred to oddlog. |
| `payload` | `?Object` | The additional payload to add to the loggers payload.

**Returns:** `this`

### getPreparedPayload

Prepares and caches the final payload of this logger (and inherited ones). According to the `singleTransform` option,
the payload might also get transformed already. The result must not be modified.

**Returns:** `?Object|undefined` - The finalized payload.

### mwExpress

Creates a middleware function to be used by express.js compatible routers. The middleware attaches a child logger to the
request object as `req.log`.

| Parameter | Type | Description |
| --- | --- | --- |
| `ownPayload` | `Boolean` | default: `this.ownChildPayloads` - Whether the payload ownership is transferred to oddlog. |
| `ownChildPayloads` | `Boolean` | default: `this.ownChildPayloads` - Fallback value for `ownPayload` parameters of methods of the child logger. |
| `childScope` | `Boolean|String` | default: `false` - If set, a child [logger scope](./logger-scope.md) will be created for the child logger. If a string is passed, it will be appended to the child logger scopes name. |
| `payload` | `?Object|{?Object}Function<req,res[,cb]>` | default: `{req, res}`. Payload of the child logger. Unless `null` is passed/returned, the parent loggers payload will be merged into this. |

**Returns:** `Function<req, res, next>` The middleware for express.js compatible routers.
