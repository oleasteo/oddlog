---
prev: false
next: false
---

# Under the hood

## Logging Levels

Each log record holds a level to represent its importance to the workflow. Although you could use any custom number
value, oddlog provides some predefined standard levels:

```
0 - SILENT
1 - TRACE
2 - DEBUG
3 - VERBOSE
4 - INFO
5 - WARN
6 - ERROR
7 - FATAL
```

It is best practice to scatter logging messages over as many levels as there are distinct *levels of significance*
within a module. If the module has some messages to tell that are more relevant than `INFO` but less relevant than
`WARN` it's completely fine to use a level like `4.5`.

We chose the ascending level in contrast to some popular logging libraries due to two reasons:

1. exit codes of programs being `0` is fine, `>=1` is error; thus ascending to error.
2. there is no possible severity level below *silent*, but one could imagine an open range upwards.

## Raw log record

At least `oddlog-cli@0.6.1` is required for the current schema.

```text
[
  {Number}  SCHEMA_VERSION, // 2
  {?String} typeKey,        // "_type"
  {?Object} meta,           // [ "4.17.8-1-ARCH", "zerg", 31522 ] // [ os.release(), os.hostname(), process.pid ]
  {String}  loggerName,     // "my-app:http"
  {String}  ISODate,        // "2018-07-22T13:28:33.572Z"
  {Number}  level,          // 4
  {?Array}  source: [       // [ "/oddlog/index.js", 42, 21 ]
    {String} file,
    {Number} row,
    {Number} col
  ],
  {?String} message,        // "the apocalypse has begun"
  {?Object} [payload],      // {"lorem":"ipsum"}
]
```

The Array syntax has been chosen due to high parsing performance and compactness. The actual record gets written part by
part instead of a single `JSON.stringify` on an actual array object; this provides the highest possible record stringify
speed as only the `meta` and `payload` fields need to be stringified generically. The order of the fields is important
since a prefix consisting of the first four fields can be calculated during logger creation thus they do not waste
computation time on a per-record basis.

The `typeKey` has been added ([#8](https://gitlab.com/frissdiegurke/oddlog/issues/8)) to provide more customization
regarding how to handle attributes of the payload within the interpreting tool (e.g. cli). By default, everything within
the payload has the default type `"plain"`; except for the top-level attributes `"err"` and `"error"`, they have the
default type `"error"`. Types can be overwritten for any object that is (part of) the payload by setting the value of
its `typeKey` attribute (e.g. `{"_type":"error"}`).
