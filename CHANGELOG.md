# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to
[Semantic Versioning](http://semver.org/). Only changes that affect the documentation may be considered to be breaking.

## Unreleased

## [2.0.0] - 2018-08-01

### Added
- TriggerTransport improved cache structure (*RingCache*) for high performance
- TriggerTransport `window` option can now be a boolean (and is by default `limit >= 4096`) for use of the new RingCache
  (`false`) instead of the slower but less memory pre-allocating RayCache (`true`)
- TriggerTransport adds `malloc` option for pre-allocation customization of its RayCache
- TriggerTransport adds `silentWindow` option to oppress cached logs above the `limit` within its RayCache
- TriggerTransport adds `freeUp` option (enabled by default) for RingCache to allow tuning up (set to `false`) `cleanUp`
  speed by not freeing up references to old logs for garbage collection until they are overwritten with new logs
- TriggerTransport adds `guard` option to allow guarding logs disable

### Changed
- Log record schema v3: Use timestamp in favor of ISO format
- TriggerTransport `window` option now defaults to `limit / 20` instead of `limit / 10` for values equal or greater than
  `4096`
- TriggerTransport flood boundary messages have changed to `--- start cache ---` and `--- end cache ---`

### Removed
- TriggerTransport no longer emits `transport:trigger:cache:drop` events on the logger.

## [1.1.0] - 2018-07-28

### Fixed
- CapTransport previously ignored the `BasicTransport` option `level`

### Added
- VoidTransport
- SplitTransport (#44)
- FileRotateTransport now accepts `compress` object for gzip options (#40)

## [1.0.2] - 2018-07-27

### Added
- Performance improvements (stringify, string escaping)

## [1.0.1] - 2018-07-25

### Fixed
- npm release did not include dist files

## [1.0.0] - 2018-07-25 [YANKED]

### Added
- Singleton store (accessible via `defineLogger` and `getLogger` (default export))
- CapTransport (#22)
- MemoryTransport
- Instance transport *alias* `{instance: Transport}` (allows passing custom Transport instances)
- Ignore `null` and `undefined` values within `transports` definition arrays (#35)
- Enable Transports to implement `Transport#attach(Logger)` for (re-)assignment of their logger
- Export `createTransport(options)` function that translates a transport definition object into a Transport instance
- `shy` option for createLogger that is intended to be used by libraries
- FileRotateTransport is now extending DeferredTransport (for messages arriving during a file rotation) (#23)
- `Logger#is<Level>` *guarding methods* that allow to check whether a log would get processed.
- `Logger#mwExpress` middleware function for express.js compatible routers; uses `Logger#child` alike parameters

### Changed
- Use `:` as scope name separator instead of `/`
- Transport constructors no longer have a `logger` parameter. Use `Transport#attach()` instead
- `Transport#child()` no longer have a `logger` parameter. Use `Transport#attach()` instead
- `oddlog.mixin()` now expects the `oddlog` instance as first parameter
- Default level and environment-based level for transports has been improved (see #34)
- Logging method calls no longer emit `error` events on the Logger, but `log-error` events instead
- Transports now must implement `{Number} Transport#minLevel()` (see #38)
- Renamed `ThresholdTransport` to `TriggerTransport`
   * Renamed `transport:threshold:drop` event on `Logger` to `transport:trigger:cache:drop`
   * Renamed `transport:threshold:initiate` event on `Logger` to `transport:trigger:flooding:start`
   * Renamed `transport:threshold:complete` event on `Logger` to `transport:trigger:flooding:end`
- Renamed `Message` to `Log`
   * Renamed `message` event on `Logger` to `log`
- Renamed std-levels export `DEFAULT` to `DEFAULT_VALUE`
- If `logSource` is not explicitly set to false, it will fallback to whether any of `DEBUG`/`TRACE` env variables apply

### Removed
- `Logger#middleware` (see `Logger#mwExpress`)
- `Logger#createTransport` - Use global export `createTransport()` (and `Transport#attach()`) instead
- `oddlog.NestedTransport` - Since it's an inherited transport without actual value, we don't want to export it
- `cheeky` option for loggers (#34 makes this obsolete)
- The sole attribute `transports` does no longer select the ThresholdTransport anymore
- Previously deprecated:
   * `oddlog.eachOnce`
   * `oddlog.clonePayload`
   * `oddlog.clonePayloadDeep`

## [0.4.0] - 2017-11-14

### Added
- Allow string type for `Logger#child:childScope` to append a suffix to the name of a child logger scope (#31, #32)

### Changed
- Alter parameter order of `Logger#child` to move `{String|Boolean} [childScope=false]` in front of the payload for more
  clear calls with sub-scope naming (e.g. `logger.child("sub-scope", { /* multi-line payload ... */})`)

## [0.3.2] - 2017-11-13

### Added
- BasicTransport option `{Boolean} shy` to omit messages unless `DEBUG` environment variable matches logger name (#30)
- BasicTransport class to share options with all standard transports

## [0.3.1] - 2017-05-11

### Added
- `Logger#mergePayload` (#27)
- `Logger#middleware` (#28)

### Fixed
- Transformer got passed stringified representation of custom objects within payload, now they get the custom object as
  expected

### Changed
- It is not searched for toString implementations during clone operation of payload anymore

### Deprecated
- `oddlog.clonePayload` - Use oddlog.clone instead
- `oddlog.clonePayloadDeep`- Use oddlog.cloneDeep instead

## [0.3.0] - 2016-12-12

### Added
- Cheeky flag for new loggers (#24)
- Attributes type field at logger creation (#8)

### Changed
- Use message record schema v1: added cheeky flag (#24), changed to static meta information (#20) and added attributes
  type field (#8)

### Deprecated
- `Logger#isClosed` - Use logger.loggerScope.closed instead

## [0.2.1] - 2016-12-12

### Added
- `oddlog.assign`

### Fixed
- `Logger#exit` default value for `code` parameter

### Deprecated
- `oddlog.eachOnce` - Method does not have any use since events on Logger are not related to each other

## [0.2.0] - 2016-12-11

### Added
- Require Transports to implement a `child` method (#16)
- `Logger#child` new parameter `childScope` to create a dedicated `LoggerScope` (#16)
- End internal streams (e.g. FileStreams) on `LoggerScope:close` event (#16)

### Fixed
- Child `Logger` without dedicated `LoggerScope` close the logger-scope on `Logger#close`
- Some methods wait for events on `Logger` that get emitted on `LoggerScope`

### Changed
- Renamed `BacktrackTransport` to `ThresholdTransport`
- Renamed `transport:backtrack:*` events to `transport:threshold:*`

### Removed
- `oddlog.eachDrainedOnce`
- `Logger#nextDrain`

## [0.1.4] - 2016-12-02

### Added
- Logger trap method for events on `process`: `Logger#handleProcessEvents` (#2)
- Helper methods simplifying trap definition: `Logger#handleUncaughtExceptions`, `Logger#handleUnhandledRejections`,
  `Logger#handleWarnings` (#2)
- `Logger#exit`
- Window size option for BacktrackTransport (#19)

### Changed
- Transports are now required to provide a `{Number} level` attribute to specify their minimal level of interest
- Performance improvements for logging functions when no Transport is interested in messages of that level (#3)

## [0.1.3] - 2016-09-20

### Fixed
- `Logger#{levelName}` methods using stateful level value

## [0.1.2] - 2016-09-20

### Added
- FileRotateTransport (#5)
- Logger and internal classes now extend EventEmitter (#17)
- Logger events: `error`, `message`, `child`, `drain`, `close`, `end`
- Logger transport events: `transport:backtrack:*`, `transport:file-rotate:*`

### Deprecated
- `oddlog.eachDrainedOnce`
- `Logger#nextDrain`

## [0.1.1] - 2016-09-19

### Changed
- Listeners for next drain are now called with `process.nextTick`
